<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>
<? $arOut = array(); ?>

<? 
	if($_REQUEST['action'] == 'change-city'){
		$userCityNew = $_REQUEST['code']; 
		$currentCode = $_SESSION['USER_CITY_CODE']; 
		if($currentCode != $userCityNew){
			$_SESSION['USER_CITY_CODE'] = $userCityNew; 	
			$arOut['change_city'] = 'Y'; 
		}else{
			$arOut['change_city'] = 'N';
		}
		
		if($_REQUEST['fuser']){
			CSaleBasket::DeleteAll($_REQUEST['fuser']);
		}

		$arOut['city'] = $userCityNew;
		$arOut['session'] = $APPLICATION->GetCurPage(); 
		
		
	}

?>

<? echo json_encode($arOut); ?>
