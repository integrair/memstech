<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;

if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY'])){
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency){?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
	<?}
}?>
<? global $USER; 
if(!$USER->IsAuthorized()):?>
<script>
	$(document).ready(function(){
		var htmlAuthStub = '<a href="/auth/?backurl='+'<?=$APPLICATION->GetCurUri(); ?>'+'" class="auth_button_catalog_block">Авторизуйтесь</a>';
		$("div.point-to-stub").html(htmlAuthStub);
	})
</script>
<? else:?>
	<script>
		$(document).ready(function(){
			/* Убираем информация об остатках для товаров которые нельзя купить*/
			$("div.button_block span.to-order").each(function(pos){
				$(this).parents("div.main_item_wrapper").find("div.item-stock").hide();
			})
		})
	</script>	
<? endif;?>