<?if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == "xmlhttprequest") die();?>
<?IncludeTemplateLangFile(__FILE__);?>
							<?if(!COptimus::IsMainPage()):?>
								</div> <?// .container?>
							<?endif;?>
						</div>
					<?if(!COptimus::IsOrderPage() && !COptimus::IsBasketPage()):?>
						</div> <?// .right_block?>
					<?endif;?>
				</div> <?// .wrapper_inner?>				
			</div> <?// #content?>
		</div><?// .wrapper?>
		<footer id="footer">
			<div class="footer_inner <?=strtolower($TEMPLATE_OPTIONS["BGCOLOR_THEME_FOOTER_SIDE"]["CURRENT_VALUE"]);?>">

				<?if($APPLICATION->GetProperty("viewed_show")=="Y" || defined("ERROR_404")):?>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/footer/comp_viewed.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>					
				<?endif;?>
				<div class="wrapper_inner">
					<div class="footer_bottom_inner">
						<div class="left_block">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"COMPONENT_TEMPLATE" => ".default",
		"PATH" => SITE_DIR."include/footer/copyright.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>							
							<div id="bx-composite-banner"></div>
						</div>
						<div class="right_block">
							<div class="middle">
								<div class="rows_block">
									<div class="item_block col-75 menus">
										<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_submenu_top", array(
											"ROOT_MENU_TYPE" => "bottom",
											"MENU_CACHE_TYPE" => "Y",
											"MENU_CACHE_TIME" => "3600000",
											"MENU_CACHE_USE_GROUPS" => "N",
											"MENU_CACHE_GET_VARS" => array(),
											"MAX_LEVEL" => "1",
											"USE_EXT" => "N",
											"DELAY" => "N",
											"ALLOW_MULTI_SELECT" => "N"
											),false
										);?>
										<div class="rows_block">
											<div class="item_block col-3">
												<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_submenu", array(
													"ROOT_MENU_TYPE" => "bottom_company",
													"MENU_CACHE_TYPE" => "Y",
													"MENU_CACHE_TIME" => "3600000",
													"MENU_CACHE_USE_GROUPS" => "N",
													"MENU_CACHE_GET_VARS" => array(),
													"MAX_LEVEL" => "1",
													"USE_EXT" => "N",
													"DELAY" => "N",
													"ALLOW_MULTI_SELECT" => "N"
													),false
												);?>
											</div>
											<div class="item_block col-3">
												<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_submenu", array(
													"ROOT_MENU_TYPE" => "bottom_info",
													"MENU_CACHE_TYPE" => "Y",
													"MENU_CACHE_TIME" => "3600000",
													"MENU_CACHE_USE_GROUPS" => "N",
													"MENU_CACHE_GET_VARS" => array(),
													"MAX_LEVEL" => "1",
													"USE_EXT" => "N",
													"DELAY" => "N",
													"ALLOW_MULTI_SELECT" => "N"
													),false
												);?>
											</div>
											<div class="item_block col-3">
												<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_submenu", array(
													"ROOT_MENU_TYPE" => "bottom_help",
													"MENU_CACHE_TYPE" => "Y",
													"MENU_CACHE_TIME" => "3600000",
													"MENU_CACHE_USE_GROUPS" => "N",
													"MENU_CACHE_GET_VARS" => array(),
													"MAX_LEVEL" => "1",
													"USE_EXT" => "N",
													"DELAY" => "N",
													"ALLOW_MULTI_SELECT" => "N"
													),false
												);?>
											</div>
										</div>
									</div>
									<div class="item_block col-4 soc">
										<div class="soc_wrapper">
											<? if($_SESSION['CITY_INFO']['PHONES'][0]):?>
												<div class="phones">
													<div class="phone_block">
														<span class="phone_wrap">
															<span class="icons fa fa-phone"></span>
															<span>
																<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
																	array(
																		"COMPONENT_TEMPLATE" => ".default",
																		"PATH" => SITE_DIR."include/phone.php",
																		"AREA_FILE_SHOW" => "file",
																		"AREA_FILE_SUFFIX" => "",
																		"AREA_FILE_RECURSIVE" => "Y",
																		"EDIT_TEMPLATE" => "standard.php",
																		"PHONE" => $_SESSION['CITY_INFO']['PHONES'][0]
																	),
																	false
																);?>
															</span>
														</span>
														<?/*?><span class="order_wrap_btn">
															<span class="callback_btn"><?=GetMessage('CALLBACK')?></span>
														</span><?*/?>
													</div>
												</div>
											<? endif;?>
											<div class="social_wrapper">
												<div class="social">
													<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
														array(
															"COMPONENT_TEMPLATE" => ".default",
															"PATH" => SITE_DIR."include/footer/social.info.optimus.default.php",
															"AREA_FILE_SHOW" => "file",
															"AREA_FILE_SUFFIX" => "",
															"AREA_FILE_RECURSIVE" => "Y",
															"EDIT_TEMPLATE" => "standard.php"
														),
														false
													);?>
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="mobile_copy">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
							array(
								"COMPONENT_TEMPLATE" => ".default",
								"PATH" => SITE_DIR."include/footer/copyright.php",
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "",
								"AREA_FILE_RECURSIVE" => "Y",
								"EDIT_TEMPLATE" => "standard.php"
							),
							false
						);?>
					</div>
					<?$APPLICATION->IncludeFile(SITE_DIR."include/bottom_include1.php", Array(), Array("MODE" => "text", "NAME" => GetMessage("ARBITRARY_1"))); ?>
					<?$APPLICATION->IncludeFile(SITE_DIR."include/bottom_include2.php", Array(), Array("MODE" => "text", "NAME" => GetMessage("ARBITRARY_2"))); ?>
				</div>
			</div>
		</footer>
		<div style="display: none;">
		    <div class="change-city-modal" id="city_list">
		        <div class="box-modal_close arcticmodal-close">закрыть</div>
		        <p class="title-change-city">Выберите ваш город.</p>
	        	<? 
		        	$dbCitys = CIBlockElement::GetList(array("SORT"=>"ASC"),array("IBLOCK_ID"=>18, "ACTIVE"=>"Y"),false,false,array("NAME","ID","CODE"));
		        	$arCitysRender = array();
		        	while($rs = $dbCitys->Fetch()){
		        		$firstLetter = strtoupper(mb_substr($rs['NAME'],0,1,'UTF-8')); 
		        		$arCitysRender[$firstLetter][] = $rs;
		        	}
		        	// ksort($arCitysRender);
	        	?>
	        	<? $isFirst = true; foreach ($arCitysRender as $letterAlphabet => $arCitysLetter):?>
	        		<!-- <p class="letter-city"><?=$letterAlphabet;?></p> -->
	        		<!-- <p class="hl"></p> -->
	        		<? foreach ($arCitysLetter as $arCity):?>
	        			<a href="#" class="city-link-change <?if($isFirst):?>first_city_link<? endif;?>" code="<?=$arCity['CODE']; ?>" id-city="<?=$arCity['ID']; ?>"><?=$arCity['NAME']; ?></a>	<br>
	        		<? endforeach; ?>
	        		<? $isFirst = false; ?>
	        	<? endforeach; ?>
				
		        
		    </div>
		</div>
		<div style="display: none;">
		    <div class="arctic-modal-confirm" id="clear-basket-confirm">
		    	<div class="box-modal_close arcticmodal-close">закрыть</div>
		        <p class="title-modal">Внимание. Ваша корзина будет сброшена.</p>
		    	<a href="#" class="confirm-button" letter="Y">Да</a>
		    	<a href="#" class="confirm-button" letter='N'>нет</a>
		    </div>
		</div>
		<?
		COptimus::setFooterTitle();
		COptimus::showFooterBasket();
		?>
	</body>
</html>