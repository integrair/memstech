<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Условия доставки");
?><h2>
<p>
 <b><span style="font-size: 12pt;">Наш интернет-магазин осуществляет доставку по Москве и регионам России: </span></b><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span><b><span style="font-size: 12pt;"> </span></b><span style="font-size: 12pt;"> </span>
<ol>
	<li><b><span style="font-size: 12pt;">Курьерская доставка по Москве 300 рублей. При заказе от 10000 рублей - бесплатно!&nbsp;</span></b></li>
	<li><b><span style="font-size: 12pt;">Самовывоз из нашего пункта выдачи или розничного магазина – бесплатно!</span></b></li>
	<li><b><span style="font-size: 12pt;">Курьерская служба Gett Delivery - от 300 до 500. Бесплатно при заказе от 30000 рублей</span></b></li>
	<li><b><span style="font-size: 12pt;">Доставка транспортной компанией СДЭК или MajorExpress по России — от 300 до 500 рублей в зависимости от региона доставки.</span></b></li>
</ol>
<span style="font-size: 12pt;"> </span><b><span style="font-size: 12pt;"> </span></b><span style="font-size: 12pt;"> </span>
<p>
	<span style="font-size: 12pt;"> </span><b><span style="font-size: 12pt;">
	Сроки доставки: </span></b><span style="font-size: 12pt;"> </span>
</p>
<span style="font-size: 12pt;"> </span><b><span style="font-size: 12pt;"> </span></b><span style="font-size: 12pt;"> </span>
<ol>
	<li><b><span style="font-size: 12pt;">Курьерская доставка по Москве при заказе до 13:00 в день заказа. При заказе после 13:00 – на следующий будний день</span></b></li>
	<li><b><span style="font-size: 12pt;">Самовывоз – в день оплаты с 10:00 до 20:00 по будням&nbsp;</span></b></li>
	<li><b><span style="font-size: 12pt;">Курьерская служба Gett Delivery - в течение 1-2 часов в день заказа (доступно с 10:00 до 20:00 по будням)</span></b></li>
	<li><b><span style="font-size: 12pt;">Доставка транспортной компанией СДЭК&nbsp;или MajorExpress по России– от 3 до 5 дней в зависимости от региона</span></b></li>
</ol>
 </h2>
<ol>
</ol><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>