<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
$APPLICATION->SetTitle('Настройка городов для обмена'); 

$APPLICATION->AddHeadString('<link href="/local/modules/multiexchange/css/style.css"  type="text/css" rel="stylesheet" />',true);

$APPLICATION->AddHeadString('<link href="/local/modules/multiexchange/js/arcticmodal/jquery.arcticmodal-0.3.css"  type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddHeadString('<link href="/local/modules/multiexchange/js/arcticmodal/themes/simple.css"  type="text/css" rel="stylesheet" />',true);

$APPLICATION->AddHeadScript('/local/modules/multiexchange/js/jquery-1.11.1.min.js');

$APPLICATION->AddHeadScript('/local/modules/multiexchange/js/arcticmodal/jquery.arcticmodal-0.3.min.js');

$APPLICATION->AddHeadScript('/local/modules/multiexchange/js/main.js');

// $aTabs = array(
//   array("DIV" => "edit1",
//         "TAB" => "Выгрузить цвета в Excel",
//         "ICON"=>"main_user_edit",
//         "TITLE"=>"Экспорт каталогов цветов в Excel"),
//   array("DIV" => "edit2",
//         "TAB" => "Обновить цвета из Excel",
//         "ICON"=>"main_user_edit",
//         "TITLE"=>"Обновление позиций и HEX-кодов из Excel"),
//   array("DIV" => "edit3",
//         "TAB" => "Обновление номенклатуры и цветов",
//         "ICON"=>"main_user_edit",
//         "TITLE"=>"Обновление цветов и номенклатуры по выгрузке из 1С")  
// );


// $tabControl = new CAdminTabControl("tabControl", $aTabs);

CModule::IncludeModule('iblock'); 

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); ?>

<div class="wrap-citys">
	<div class="information"></div>
	<div class="create-new-city">
		<h3>Добавить новый город:</h3>

		<form action="" class="create-city">
			
			<p>
				<label for="name_new_city" class="lable-create-inputs"><b>Название нового города:</b></label>
				<input type="text" id="name_new_city" name="name_new_city" />
			</p>
			<p>
				<label for="code_new_city" class="lable-create-inputs"><b>Символьный код города (поддомен):</b></label>
				<input type="text" id="code_new_city" name="code_new_city" />
			</p>			
			<p>
				<label for="login_new_city" class="lable-create-inputs">Логин в 'Мой Склад': </label>
				<input type="text" id="login_new_city" name="login_new_city" />
			</p>
			
			<p>
				<label for="pass_new_city" class="lable-create-inputs">Пароль в 'Мой Склад':</label>
				<input type="text" id="pass_new_city" name="pass_new_city" />
			</p>
			<br>
			<input type="submit" class="adm-btn-save" name="adm-btn-save" id="create-button" value="Добавить город" />
			
<!-- 			<a href="#">Проверить доступы</a>

<label for="check_synchronization">Включить обмен</label>
<input type="checkbox" name="check_synchronization" id="check_synchronization"> -->


		</form>
	</div>
	<div class="city-list">
		<h3 class="title-list-items">Добавленные города:</h3>
	<?  $dbCitys = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>18,),false,false,array("ID","NAME","PROPERTY_LOGIN","PROPERTY_PASSWORD","PROPERTY_CHECK_SYNCH","PROPERTY_MAIN_CITY"));
		while($rc = $dbCitys->Fetch()):?>
			<div class="item-city-list">
				<h3><?=$rc['NAME']; ?></h3>
				<input type="hidden" name="id_current_item[]" class="id_city" value="<?=$rc['ID']; ?>">
				<p>
					Логин в "Мой склад": <span class="login-place"><?=$rc['PROPERTY_LOGIN_VALUE']; ?></span>
					<span class="input-login">
						<input type="text" class="login-field" value="<?=$rc['PROPERTY_LOGIN_VALUE']; ?>" />
					</span>
				</p>
				<p>
					Пароль в "Мой склад": <span class="password-place"><?=$rc['PROPERTY_PASSWORD_VALUE']; ?></span>
					<span class="input-password">
						<input type="text" class="password-field" value="<?=$rc['PROPERTY_PASSWORD_VALUE']; ?>" />
					</span>
				</p>
				<p>
					<label for="check_synchronization">Включить обмен</label>
					<input type="checkbox" name="check_synchronization" <?if($rc["PROPERTY_CHECK_SYNCH_ENUM_ID"] == 30):?>checked<?endif;?> class="check_synchronization">
				</p>
				<?if(!$rc['PROPERTY_MAIN_CITY_VALUE']):?>
					<p>
						<a href="#" class="bind-stores">Привязка складов</a>
					</p>
					<p>
						<a href="#" class="bind-prices">Привязка цен</a>
					</p>
				<? endif;?>

				<p class="main-block">
					<a href="#" class="adm-btn-save city-button test-connect-city">Проверить соединение</a><span class="test_connect_info">Соединение установлено</span>				
				</p>

				<!-- <p class="main-block">
					<a href="#" class="adm-btn-save city-button unload-ms">Выгрузить товары в МС</a>	
				</p> -->

				

				<p class="main-block">
					<a href="#" class="adm-btn-save city-button change-city">Изменить параметры города</a>
				</p>

				<p class="main-block">
					<a href="#" class="adm-btn-save city-button delete-city">Удалить город</a>
				</p>

				<p class="temp-block">
					<a href="#" class="adm-btn-save city-button save-city-change">Сохранить</a>
				</p>

				
			</div>
		<? endwhile;?>
		
	</div>
</div>

<div style="display:none; ">
	<div id="modal-for-binds" class="modal-window-property">
		<a href="#" class="arcticmodal-close close-button">X</a>
		<div class="ajax-container-binds"></div>
	</div>
</div>


<div class="wrap-control-flags">
	<h2>Флаги обмена</h2>
	<div class="control-flags-list">

		
	</div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>