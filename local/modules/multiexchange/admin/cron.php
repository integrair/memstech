<?
	$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__).'/../../../..');
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

	define("NO_KEEP_STATISTIC", true);
	define("NOT_CHECK_PERMISSIONS",true);
	define('BX_CRONTAB', true);
	//define('CHK_EVENT', true);



	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" );

	register_shutdown_function('shutdownScriptSubHundler');

	function shutdownScriptSubHundler() {
		
		$error = error_get_last();
		if($error['type']){
			MSETools::setFlag('errorInfo', print_r($error, true));	
		}
    	

	}

	


	@set_time_limit(0);
	@ignore_user_abort(true);
	CModule::IncludeModule("highloadblock");
	use Bitrix\Highloadblock as HL;
	use Bitrix\Main\Entity;

echo '<pre>';
	if(CModule::IncludeModule('iblock') && 
		CModule::IncludeModule('catalog') && 
		CModule::IncludeModule('sale') && 
		CModule::IncludeModule('multiexchange')){
		$objWorkLog = new MSEXchangeLog();

		$cntBlock = MSETools::getFlag('block_cnt'); 

		if((MSETools::getFlag('block') == 'N') || ($cntBlock > 10))  {
			
			MSETools::setFlag('block',"Y");
			MSETools::setFlag('block_cnt',false);
			// $objWorkLog = new MSEXchangeLog();


			/* Определяем текущий город для загрузки */
			if(!MSETools::getFlag('city')){
				
				$arCityUpdate = array();
				
				$dbCity = CIBlockElement::GetList(
					array(),
					array(
						"IBLOCK_ID"=>18, 'PROPERTY_CHECK_SYNCH'=>30),
					false,
					false,
					array(
						"ID",
						"NAME",
						"PROPERTY_LOGIN",
						"PROPERTY_PASSWORD",
						"CODE",
						"PROPERTY_MAIN_CITY",
						"PROPERTY_PRICE",
						"PROPERTY_MS_BINDS_PRICES",
                        "PROPERTY_MS_BINDS_STORES"
					)
				);

				while($rc = $dbCity->Fetch()){

					if($rc['PROPERTY_MAIN_CITY_ENUM_ID'] == 31){

						MSETools::setFlag('main_access',array(
							"LOGIN"=> $rc['PROPERTY_LOGIN_VALUE'] ,
							"PASSWORD"=> $rc['PROPERTY_PASSWORD_VALUE']
							)
						);

					}else{
						$arCityUpdate[] = array(
							'ID'=>$rc['ID'],
							"NAME"=>$rc['NAME'],
							"CODE"=>$rc['CODE'],
							"LOGIN"=>$rc['PROPERTY_LOGIN_VALUE'],
							"PASSWORD"=>$rc['PROPERTY_PASSWORD_VALUE'],
							"TYPE_PRICE"=> $rc['PROPERTY_PRICE_VALUE'],
							"BIND_PRICES" => ($rc['PROPERTY_MS_BINDS_PRICES_VALUE'] ? unserialize(base64_decode($rc['PROPERTY_MS_BINDS_PRICES_VALUE'])) : array()),
							"BIND_STORES" => ($rc['PROPERTY_MS_BINDS_STORES_VALUE'] ? unserialize(base64_decode($rc['PROPERTY_MS_BINDS_STORES_VALUE'])) : array())
						);	
					}
				}

				MSETools::setFlag('arCity',$arCityUpdate);
				MSETools::setFlag('city', $arCityUpdate[0]['CODE']);
				MSETools::setFlag('currentCity', 0);
				
			}else{
				MSETools::getFlag('city');
			}
			/* END Определяем текущий город для загрузки */

			if(!MSETools::getFlag('step')){
				MSETools::setFlag('step','sections');
				$objWorkLog->addNewLogLine('Начало загрузки разделов');
			}

			$currentStep = MSETools::getFlag('step');
			
			$iblockCatalog = 22; 

			if($currentStep == 'sections'){
			
				$mainAccess = MSETools::getFlag("main_access");
				$arCitys = MSETools::getFlag("arCity");
				$currentPos = MSETools::getFlag("currentCity");
				$objWorkLog->addNewLogLine('Загрузка разделов в '.$arCitys[$currentPos]['NAME']);

				$objConnectMain = new MSExchange($mainAccess['LOGIN'],$mainAccess['PASSWORD'], $iblockCatalog);

				$objConnect = new MSExchange($arCitys[$currentPos]['LOGIN'], $arCitys[$currentPos]['PASSWORD'], $iblockCatalog);

				$objConnectMain->getInfoSectionSite();

				// print_r($arCitys[$currentPos]['CODE']);
				$objConnectMain->createBindsCodesArray(false, 'root');
				$objConnect->createBindsCodesArray(false, $arCitys[$currentPos]['CODE']);

				// print_r($objConnectMain->arSectionSiteInfo); 
				// print_r(array_keys($objConnectMain->arSectionMSInfo)); 
				// print_r(array_keys($objConnect->arSectionMSInfo)); 
				// print_r($objConnect->arSectionMSInfo);
				// print_r('-------------------');
				// print_r('--------------------------------');


				// print_r(count($objConnectMain->arSectionSiteInfo));
					// print_r('<br />');
				// print_r(count($objConnect->arSectionMSInfo));
				// print_r('<br />'); 

				MSExchange::recursiveUpdateSection(
					$objConnectMain->arSectionSiteInfo, 
					true, 
					$arCitys[$currentPos]['LOGIN'], 
					$arCitys[$currentPos]['PASSWORD'], 
					$objConnect->arSectionMSInfo
				);

				$nextCityPos = $currentPos + 1; 
				if($arCitys[$nextCityPos]){
					MSETools::setFlag('city', $arCitys[$nextCityPos]['CODE']);
					MSETools::setFlag('currentCity', $nextCityPos);
				}else{
					MSETools::setFlag('step','stocks');
					MSETools::setFlag('offset', 1);
					MSETools::setFlag('city', $arCitys[0]['CODE']);
					MSETools::setFlag('currentCity', 0);
					$objWorkLog->addNewLogLine('Начало загрузки остатков во временную таблицу');
				}
				
				MSETools::setFlag('block',"N");

				// print_r('END SECTION'); 
			
			}elseif($currentStep == 'stocks'){

				$arCitys = MSETools::getFlag("arCity");
				$currentPos = MSETools::getFlag("currentCity");
				$objWorkLog->addNewLogLine('Загрузка остатков во временную таблицу для '.$arCitys[$currentPos]['NAME']);

				$stepStocksLinks = 3; 
				$objConnect = new MSExchange($arCitys[$currentPos]['LOGIN'], $arCitys[$currentPos]['PASSWORD'], $iblockCatalog);
				
				$nextLinkST = MSETools::getFlag("next_link_stocks");

				$xmlStore = $arCitys[$currentPos]['BIND_STORES']['ms'];

				if(!$nextLinkST){
					$resultFinal = $objConnect->getRecursiveAllStocks(false, $xmlStore, 3);
				}else{
					$resultFinal = $objConnect->getRecursiveAllStocks($nextLinkST, $xmlStore, 3);	
				}


				if(!empty(MSExchange::$arFinalStocksBinds)){
					foreach (MSExchange::$arFinalStocksBinds as $xmlIDItem => $amount) {

						// Запись остатков во временную БД
						MSExchange::writeNewItemCityToTDB($xmlIDItem, $arCitys[$currentPos]['ID'], $amount);
					}
				}

				
				if(MSExchange::$nextLinkStocks){
					MSETools::setFlag('next_link_stocks', MSExchange::$nextLinkStocks);
					$objWorkLog->addNewLogLine('Следующая ссылка '.MSExchange::$nextLinkStocks);
				}else{
					$nextCityPos = $currentPos + 1;
					
					MSETools::setFlag('next_link_stocks', false);

					if($arCitys[$nextCityPos]){
						MSETools::setFlag('city', $arCitys[$nextCityPos]['CODE']);
						MSETools::setFlag('currentCity', $nextCityPos);
					}else{
						MSETools::setFlag('step','items');
						// MSETools::setFlag('offset', 1);
						MSETools::setFlag('city', $arCitys[0]['CODE']);
						MSETools::setFlag('currentCity', 0);
						$objWorkLog->addNewLogLine('Начало загрузки товаров');
					}
				}
                
                MSETools::setFlag('block',"N");




			}elseif($currentStep == 'items'){
				
				$currentOffset = MSETools::getFlag('offset');
				$stepExchange = 350; 
				$mainAccess = MSETools::getFlag("main_access");
				$arCitys = MSETools::getFlag("arCity");
				$currentPos = MSETools::getFlag("currentCity");

				$objWorkLog->addNewLogLine('Загрузка товаров для города '.$arCitys[$currentPos]['NAME']);
				$objWorkLog->addNewLogLine('Смещение '.$currentOffset);

				$cityTypePrice = $arCitys[$currentPos]['TYPE_PRICE'];

				$objConnectMain = new MSExchange($mainAccess['LOGIN'],$mainAccess['PASSWORD'], $iblockCatalog);
				$objConnect = new MSExchange($arCitys[$currentPos]['LOGIN'], $arCitys[$currentPos]['PASSWORD'], $iblockCatalog);

				$objConnect->createBindsCodesArray(false, $arCitys[$currentPos]['CODE']);
				MSEItems::getSectionsInfoSite($iblockCatalog);
				MSEItems::getItemsSite($iblockCatalog, $stepExchange, $currentOffset, $cityTypePrice);

				$objCityConnectCurrency = new MSExchange($arCitys[$currentPos]['LOGIN'], $arCitys[$currentPos]['PASSWORD'], $iblockCatalog);
				
				$allTypesPriceMainMS = $objConnectMain->getAllTypesPriceMS(); // Названия всех типов цен в Москве 
				

				$allTypesPriceCityMS = $objConnect->getAllTypesPriceMS(); // Названия всех типов цен в текущем городе

				$arCurrentcyMS = $objCityConnectCurrency->getArMainCurrency('RUB');
				$hrefCurrentcyMS = $arCurrentcyMS['href'];

				$arAltCurrency = $objCityConnectCurrency->getArMainCurrency('CNY');
				$hrefAltCurrency = $arAltCurrency['href']; 

				unset($objCityConnectCurrency);

				if(MSEItems::$arSiteItems){
					
					$insideCounter = $stepExchange * ($currentOffset - 1); 
					
					$objCounter = new MSCounterItemCity();

					foreach (MSEItems::$arSiteItems as $ext => $arItem){

						// if($objCounter->sqlite_checkItemCity($arItem['ID'], $arCitys[$currentPos]['CODE'])) continue; 
						
						/* Данные из основного аккаунта которые не пришли */
						
						$arItemMSMain = $objConnectMain->getItemsInfoMS($arItem['EXT']);
						


						$weightMainMS = $arItemMSMain['weight'];
						
						if($weightMainMS){
							$arItem['WEIGHT'] = $weightMainMS; 
							CCatalogProduct::Update($arItem['ID'], array('WEIGHT' => $weightMainMS));
						}else{
							$arItem['WEIGHT'] = false;
							CCatalogProduct::Update($arItem['ID'], array('WEIGHT' => 0));
						}

						$volumeMainMS = $arItemMSMain['volume'];
						
						if($volumeMainMS){
							$arItem['VOLUME'] = $volumeMainMS;
							CIBlockElement::SetPropertyValuesEx($arItem['ID'], $iblockCatalog, array("VOLUME"=> $volumeMainMS));
						}else{
							$arItem['VOLUME'] = false;
							CIBlockElement::SetPropertyValuesEx($arItem['ID'], $iblockCatalog, array("VOLUME"=> false));
						}

						/* Для картинок */

						if($arItemMSMain['image']['miniature']['href']){
							
							/* Загружаем картинку на сайт */
							/*$fileNameImage = $arItemMSMain['image']['filename'];
				            $fileNameSite = $_SERVER['DOCUMENT_ROOT'].'/temp/'.$fileNameImage; 
				            $contentImage = $objConnectMain->simpleGetToURL($arItemMSMain['image']['miniature']['href']);
				            file_put_contents($fileNameSite, $contentImage);

				            $objElement = new CIBlockElement;
				            $objElement->Update($arItem['ID'], array("DETAIL_PICTURE" => CFile::MakeFileArray($fileNameSite)));
				            
				            $imageBase64 = base64_encode(file_get_contents($fileNameSite));
				            unlink($fileNameSite); */

				            /* END Загружаем картинку на сайт */

						}

						/* END Для картинок */

						// if($arItemMSMain['image'] && !empty($arItemMSMain['image'])){

						// 	$imgMainMS = $arItemMSMain['image'];
						// 	$filename = $imgMainMS['filename'];
						// 	$linkImage = $imgMainMS['miniature']['href']; 

						// }
						

						/* END Данные из основного аккаунта которые не пришли */


						$arItem['currency'] = $arCurrentcyMS;
						$arItem['alt_currency'] = $arAltCurrency; 

						// $arItem['content_img'] = $imageBase64;
						// $arItem['name_img'] = $fileNameImage;


						$objItem = new MSEItems($arItem, $objConnect->arSectionMSInfo); 
						$objItem->getCurrentPriceSite($arItem['ID'], $arCitys[$currentPos]['TYPE_PRICE'], $allTypesPriceMainMS, $allTypesPriceCityMS);
						$objItem->setBuyPriceSite($arItem['ID']);

						$login = $arCitys[$currentPos]['LOGIN'];
						$password = $arCitys[$currentPos]['PASSWORD'];

						$bindStores = $arCitys[$currentPos]['BIND_STORES'];
						$bindPrices = $arCitys[$currentPos]['BIND_PRICES'];

						$objCityConnect = new MSExchange($login, $password, $iblockCatalog);

						// $arItemMSTest = $objCityConnect->getItemsInfoMS($ext);

						if($idItemMS = $objCityConnect->isItemsExistInMS($ext)){
						// if($idItemMS = $objCityConnect->isItemsExistInMS($ext)){

							$resultUpdateItem = $objItem->Update($objCityConnect, $idItemMS);
							/* Обмен ценами и остатками для каждого города ****************************************************/
							
							unset($objCityConnect);
							$objCityConnect = new MSExchange($login, $password, $iblockCatalog);
							$urlItem = 'https://online.moysklad.ru/api/remap/1.1/entity/product/'.$idItemMS;
							$itemAfter2Update = $objCityConnect->getRequest($urlItem);							
							

							if(!empty($itemAfter2Update['salePrices'])){
								foreach($itemAfter2Update['salePrices'] as $arPriceMsForUpdate) {
									foreach ($bindPrices as $coupleBindPrice) {
										if($arPriceMsForUpdate['priceType'] == $coupleBindPrice['ms']){
											
											$xmlPriceSite = $coupleBindPrice['site'];
											$valuePrice = $arPriceMsForUpdate['value'] / 100;
											if($arPriceMsForUpdate['currency']['meta']['href'] == $hrefAltCurrency){
												$currency = 'CNY';
											}else{
												$currency = 'RUB';
											}

											MSETools::addItemPrice($arItem['ID'], $xmlPriceSite, $valuePrice, $currency);

										}
									}
								}
							}

							/* Остатки */

							if($itemAfter2Update['id']){
								
								$amountCity = MSExchange::getAmountCityToXML_ID($itemAfter2Update['id'], $arCitys[$currentPos]['ID']); 
								if($amountCity){
									MSExchange::updateAmountStoreProduct($arItem['ID'], $arCitys[$currentPos]['BIND_STORES']['site'], $amountCity);
								}else{
									MSExchange::resetAmountToStoreProduct($arItem['ID'], $arCitys[$currentPos]['BIND_STORES']['site']);
								}

								MSExchange::deleteItemCityTDB($itemAfter2Update['id'], $arCitys[$currentPos]['ID']); 

							}

							/* END Остатки */


							/* END Обмен ценами и остатками для каждого города ***********************************************/

						}else{

							$resultAddItem = $objItem->Add($objCityConnect);
						}
						
						$insideCounter++; 
						MSETools::setFlag('countElementsUpdate', $insideCounter);

						unset($imageBase64);
						unset($objCityConnect); 

						$objCounter->sqlite_addNewItemCity($arItem['ID'], $arCitys[$currentPos]['CODE']);


					}

					MSETools::setFlag('offset', $currentOffset + 1);
					MSETools::setFlag('block',"N");

				}else{
					$nextCityPos = $currentPos + 1; 
					if($arCitys[$nextCityPos]){
						MSETools::setFlag('city', $arCitys[$nextCityPos]['CODE']);
						MSETools::setFlag('currentCity', $nextCityPos);
						MSETools::setFlag('offset',1);
						MSETools::setFlag('block',"N");
					}else{
						MSETools::setFlag('step','end');
						MSETools::setFlag('offset',1);
						MSETools::setFlag('city', $arCitys[0]['CODE']);
						MSETools::setFlag('currentCity', 0);
						$objWorkLog->addNewLogLine('Обмен разделами, товарами и ценами завершен');
					}
				}
			}
		}else{

			$currentCntBlock = MSETools::getFlag('block_cnt'); 

			if($currentCntBlock){
				MSETools::setFlag('block_cnt', ++$currentCntBlock);  
			}else{
				MSETools::setFlag('block_cnt', 1);  
			}
			
		}
		
	}

	// header( "refresh:2; url=/bitrix/admin/ms_json_cron.php" );

	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php" ) ;


?>