<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
$APPLICATION->SetTitle('Журнал обмена с Мой склад'); 

$APPLICATION->AddHeadString('<link href="/local/modules/multiexchange/css/style.css"  type="text/css" rel="stylesheet" />',true);

CModule::IncludeModule('iblock'); 

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); ?>


<div class="wrap-log-list">
	<? $objLog = new MSEXchangeLog();
	$arLog = $objLog->printLogTest(); 
	if(!empty($arLog)):?>
		<table>
		<? foreach($arLog as $lineLog):?>
			<? if(
			   strpos($lineLog['MESSAGE'], "аказ") > 0

				):?>
				
				<? continue; ?>

			<? endif; ?>
				<tr>
					<td><b><?=$lineLog['DATA']; ?></b></td>
					<td><b><?=$lineLog['MESSAGE']; ?></b></td>
				</tr>
			
				
		<? endforeach;?>
		</table>
	<?else:?>
		<b>В журнале пока нет записей. </b>
	<? endif; ?>

</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>