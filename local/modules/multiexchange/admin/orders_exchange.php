<?
	$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__).'/../../../..');
	$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

	define("NO_KEEP_STATISTIC", true);
	define("NOT_CHECK_PERMISSIONS",true);
	define('BX_CRONTAB', true);
	//define('CHK_EVENT', true);



	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php" );


	@set_time_limit(0);
	@ignore_user_abort(true);

echo '<pre>';
	if(CModule::IncludeModule('iblock') && 
		CModule::IncludeModule('catalog') && 
		CModule::IncludeModule('sale') && 
		CModule::IncludeModule('multiexchange')){

		$objWorkLog = new MSEXchangeLog();
		
		/* Заказы сайт */
		print_r(MSETools::getFlag('block_order'));

		$cntOrderBlock = MSETools::getFlag('cnt_block_orders');
		if((MSETools::getFlag('block_order') != 'Y') || ($cntOrderBlock > 15)){

			$objWorkLog->addNewLogLine('Начало загрузки заказов c сайта в МС');
			MSETools::setFlag('block_order',"Y");
			MSETools::setFlag('cnt_block_orders',false);

			$arActualOrdersProcessing = MSETools::getActualOrdersProcessing();

			if(!empty($arActualOrdersProcessing)){
			    foreach ($arActualOrdersProcessing as $idOrder){
			    	
			    	$objWorkLog->addNewLogLine('Начало выгрузки заказа '.$idOrder);

			        $locationProps = CSaleOrderPropsValue::GetList(array(),
			            array("ORDER_ID" => $idOrder,'CODE'=>"LOCATION"),false, false, array("VALUE"))->Fetch();

			        if($locationProps['VALUE']){

			            $dbMainCitySynch = CIBlockElement::GetList(array(),
			                array("IBLOCK_ID"=>18, "PROPERTY_LOCATION" => $locationProps['VALUE'], "ACTIVE"=>"Y"),false,false, 
			                array("ID","PROPERTY_LOGIN","PROPERTY_PASSWORD"))->Fetch();

			            $login = $dbMainCitySynch['PROPERTY_LOGIN_VALUE'];
			            $password = $dbMainCitySynch['PROPERTY_PASSWORD_VALUE'];
			            $objConnectOrder = new MSOrdersExchange($login, $password);
			            // print_r($idOrder);

			            $msOrderInfo = $objConnectOrder->isExistOrder($idOrder);
			            
			            // print_r($msOrderInfo);

			            if(empty($msOrderInfo)){
	                		
			                /* Информация о заказе с сайта */
			                $dbOrder = CSaleOrder::GetByID($idOrder);
			                $payOrder = $dbOrder['PAYED']; 
			                $deliveryPrice = $dbOrder['PRICE_DELIVERY'];
			                $idUser = $dbOrder['USER_ID'];
			                $dbUser = CUser::GetByID($idUser)->Fetch();
			                $xmlIDUser = $dbUser['XML_ID']; 
			                $firstName = $dbUser['NAME']; 
			                $emailUser = $dbUser['EMAIL']; 
			                $lastName = $dbUser['LAST_NAME']; 
			                
			                $organizationTitle = $dbUser['UF_ORGANIZATION'];

			                $letterStatusSite = $dbOrder['STATUS_ID'];
			                $arStatesMS = $objConnectOrder->getMSStatesLetters(); 



			                $dbBasket = \Bitrix\Sale\Basket::getList(array('filter' => array('ORDER_ID' => $idOrder)));
			                $arItemsBasket = array();
			                while($bItem = $dbBasket->Fetch()){
			                     $arItemsBasket[$bItem['ID']] = array(
			                           'NAME' => $bItem['NAME'],
			                          'PRICE' => (float)$bItem['PRICE']*100,
			                          'QUANTITY' => (float)$bItem['QUANTITY'],
			                          'XML_ID' => $bItem['PRODUCT_XML_ID']
			                     );
			                }
			                /* END Информация о заказе с сайта */

			                $objConnectOrder->setOrganization(); // Устанавливаем текущую организацию МС

			                /*** Тело для создания заказа **/
			                $objConnectOrder->bodyOrder['name'] = str_pad($dbOrder['ID'], 5, '0', STR_PAD_LEFT);
			                $objConnectOrder->bodyOrder['externalCode'] = $dbOrder['ID'];
			                $objConnectOrder->bodyOrder['sum'] = $dbOrder['PRICE']; 
			                $objConnectOrder->bodyOrder['organization']['meta'] = $objConnectOrder->msOrganization; 
			                
			                if($idMSState = array_search($letterStatusSite, $arStatesMS)){
	            				$objConnectOrder->bodyOrder['state'] = array(
	            					'meta' => array(
	            						'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/customerorder/metadata/states/'.$idMSState,
	            						'type' => 'state',
      									"mediaType" => "application/json"
	            					)
	            				);
	            			}               

			                $resultContractorTest = $objConnectOrder->isContractor($xmlIDUser); // Проверка существования контрагента по внешнему коду
			                // print_r($resultContractorTest); 


			                $isCreateContractor = false;
			                if(!$resultContractorTest){

			                    $objConnectOrder->bodyContractor['name'] = $firstName;/*." ".$lastName*/
			                    $objConnectOrder->bodyContractor['externalCode'] = (string)$xmlIDUser;
			                    $objConnectOrder->bodyContractor['email'] = $emailUser;
			                    if($organizationTitle){
			                    	$objConnectOrder->bodyContractor['name'] = (string)$organizationTitle;
			                    	$objConnectOrder->bodyContractor['legalTitle'] = (string)$organizationTitle;	
			                    }
			                    

			                    $contractorInfo = $objConnectOrder->createContractor();
			                    
			                    // print_r($contractorInfo);

			                    $agentInfo = $contractorInfo['meta'];
			                    $isCreateContractor = true; 

			                }else{
			                    $agentInfo = $resultContractorTest['meta']; 
			                }

			                $objConnectOrder->bodyOrder['agent']['meta'] = $agentInfo;

			                if($isCreateContractor){ // Если создали контрагент, пересоздаем подключение
			                    $bufferVar = $objConnectOrder->bodyOrder; 
			                    unset($objConnectOrder);
			                    $objConnectOrder = new MSOrdersExchange($login, $password);
			                    $objConnectOrder->bodyOrder = $bufferVar; 
			                }

			                
			                // print_r($objConnectOrder);
			                $objWorkLog->addNewLogLine('Создаем новый заказ в МС '.$idOrder);
			                $resultCreateOrder = $objConnectOrder->createNewOrder(); 

			                unset($objConnectOrder);

			                if($payOrder == 'Y'){
			                	$objConnectOrder = new MSOrdersExchange($login, $password);
			                	$objConnectOrder->createPayDoc($resultCreateOrder, $idOrder);
			                	unset($objConnectOrder);
			                }

			                $idOrderMS = $resultCreateOrder['id'];
			                $objConnectOrder = new MSOrdersExchange($login, $password);
			                $arDeliveryMS = $objConnectOrder->getDeliveryExt();
			                unset($objConnectOrder);
			                $objConnectOrder = new MSOrdersExchange($login, $password);

			                if(!empty($arItemsBasket)){ // Добавляем позиции для заказа
			                    
			                    foreach ($arItemsBasket as $idSite => $arItemSite) {
			                        
			                        $itemMS = $objConnectOrder->testItemInMS($arItemSite['XML_ID']);
			                        $objConnectOrder->bodyPositions[] = array(
			                            "quantity" => $arItemSite['QUANTITY'],
			                            "price" => $arItemSite['PRICE'],
			                            "assortment" => array(
			                                'meta' => $itemMS['meta']
			                            ),
			                        );
			                    }



			                    if($deliveryPrice){
			                    	$objConnectOrder->bodyPositions[] = array(
			                            "quantity" => 1,
			                            "price" => $deliveryPrice * 100,
			                            "assortment" => array(
			                            	"meta" => $arDeliveryMS['meta']
			                            )
			                        );
			                    }

			                    $objConnectOrder->addNewPositionInOrder($idOrderMS); 
			                    
			                } // END Добавляем позиции для заказа
			                MSETools::delOrderFromProcessing($idOrder);
			            }else{
			            	/* Уже существует */

			            	$dbOrder = CSaleOrder::GetByID($idOrder);
			            	$dateUpdateSite = $dbOrder['DATE_UPDATE'];
			            	$dateUpdateMS = $msOrderInfo['updated']; 
			            	$letterStatusSite = $dbOrder['STATUS_ID']; 
			                
			            	$compareDate = MSOrdersExchange::compareTwoDateMicroSeconds($dateUpdateMS, $dateUpdateSite, false);
			            	
			            	if($compareDate < 0){
			            		$arStatesMS = $objConnectOrder->getMSStatesLetters(); 
			            		if($idMSState = array_search($letterStatusSite, $arStatesMS)){
			            			$currentMSState = explode("/", $msOrderInfo['state']['meta']['href']);
			            			$currentMSState = $currentMSState[count($currentMSState) - 1]; 
			            			$letterStatusMS = $arStatesMS[$currentMSState];
			            			if($letterStatusMS != $letterStatusSite){
			            				$objConnectOrder->updateStateInMS($idMSState, $msOrderInfo);
			            				unset($objConnectOrder); 
			            				$objConnectOrder = new MSOrdersExchange($login, $password);
			            			}




			            		}
			            	}

			            	$orderPayed = $dbOrder['PAYED'];

			            	if($orderPayed == 'Y'){
			            		$objConnectOrder->createPayDoc($msOrderInfo, $idOrder); 
			            	}
			            	
			                MSETools::delOrderFromProcessing($idOrder);
			            } //////// END if msOrderInfo
			        }else{
			        	MSETools::delOrderFromProcessing($idOrder);
			        }
			    }
			}else{
				$objWorkLog->addNewLogLine('Заказов для обмена пока нет');
			}



			/******************* МС -> Сайт ***************************************/

				$dbLocations = CIBlockElement::GetList(array(),
		                array("IBLOCK_ID"=>18,"ACTIVE" => "Y"), false,false, 
		                array("ID","PROPERTY_LOGIN","PROPERTY_PASSWORD","PROPERTY_LOCATION"));
				$objWorkLog->addNewLogLine('Начало загрузки заказов с МС на сайт');
				while($rl = $dbLocations->Fetch()){
				    
				    $idCity = $rl['ID'];
				    $loginCity = $rl['PROPERTY_LOGIN_VALUE'];
				    $passCity = $rl['PROPERTY_PASSWORD_VALUE'];
				    $locationCity = $rl['PROPERTY_LOCATION_VALUE']; 

				    $objConnectOrder = new MSExchange($loginCity, $passCity, 22);
				    $arMSLastOrders = $objConnectOrder->getLastUdatedOrdersMS(10); 
				    

				    if(!empty($arMSLastOrders['rows'])){
				        foreach ($arMSLastOrders['rows'] as $arMSOrder){

				        	$objWorkLog->addNewLogLine('Выгрузка заказа '.$arMSOrder['name']);

				            $arMSOrderShort = array(
				                'NAME' => $arMSOrder['name'],
				                'XML_ID' => $arMSOrder['externalCode'],
				                'SUM' => $arMSOrder['sum'],
				            );

				            $msAgent = $arMSOrder['agent']['meta']['href'];
				            $msPosition = $arMSOrder['positions']['meta']['href'];
				            $msStatus = $arMSOrder['state']['meta']['href'];
				            
				            $connect = new MSOrdersExchange($loginCity, $passCity); 
				            $infoMSAgent = $connect->getUseMSInfo($msAgent);
				            $arMSOrderShort['USER'] = $infoMSAgent;
				            
				            $msStateVal = $connect->getCurrentStateMS($msStatus);
				            $arMSOrderShort['STATUS'] = $msStateVal;
				            $arPositionsMS = $connect->getPositionsMS($msPosition);
				            $arMSOrderShort['BASKET'] = $arPositionsMS;
				            $stateMSHref = $arMSOrder['state']['meta']['href'];
				            $arStateMSInfo = explode('/', $stateMSHref); 
				            $myStateID = $arStateMSInfo[count($arStateMSInfo) -1];

				            $arCurrentMsStates = $connect->getMSStates();
				            $nameStateMS = $arCurrentMsStates[$myStateID];
				            $lettersState = preg_replace('/^(\[(.+)\])?.+$/', '\2', $nameStateMS); 

				            $dbStateSite = CSaleStatus::GetByID($lettersState);
				           
				            $arOrderSite = MSOrdersExchange::isOrderExistOnSite($arMSOrderShort['XML_ID']);

				            if($arOrderSite){
				            	print_r('Заказ существует'); 
				            	
				            	$urlMSAgent = $msAgent;
				            	$arMSAgent = $connect->getRequest($urlMSAgent); 
				            	
				            	$idAgentSite = $arOrderSite['USER_ID'];
				            	$arAgentSite = CUser::GetByID($idAgentSite)->Fetch();
				            

				            	if($arAgentSite['XML_ID'] != $arMSAgent['externalCode']){

				            		//$dbUser = CUser::GetList(array(),array("XML_ID" => $arMSAgent['externalCode']),false,false,array("ID"))->Fetch();
				            		$dbUser = CUser::GetList(
										($by="personal_country"),
										($order="desc"),
										array("XML_ID"=>$arMSAgent['externalCode']),
										array("FIELDS"=>array("ID","NAME","EMAIL","LAST_NAME")))->Fetch();


				            		if($dbUser['ID']){
				            			/* Обновление пользователя */

						                $arFieldsUserUpdate = array(
						                  "XML_ID" => $arMSAgent['externalCode']
						                );
										$connect->addNewCounterpartySite($arFieldsUserUpdate);

				            			/* END */
				            		
				            			$arFieldsUpdateUser = array(
									      "USER_ID" => $dbUser['ID'],
									    );

									   CSaleOrder::Update($arOrderSite['ID'], $arFieldsUpdateUser);

				            		}else{
				            			/**/

										
										$newPassword = MSOrdersExchange::generatePassword(); 
				                
						                $name = $arMSAgent['name'];
						                $arParams = array("replace_space"=>"","replace_other"=>"");
						                $stubEmail = Cutil::translit($name,"ru",$arParams);
						                $stubEmail .= '@test.tx'; 

						                $arFieldsUser = Array(
						                  "NAME"              => $name,
						                  "EMAIL"             => $arMSAgent['email'] ? $arMSAgent['email'] : $stubEmail,
						                  "LOGIN"             => $arMSAgent['email'] ? $arMSAgent['email'] : $stubEmail,
						                  "LID"               => "s1",
						                  "ACTIVE"            => "Y",
						                  "GROUP_ID"          => array(2,3,6),
						                  "PASSWORD"          => $newPassword,
						                  "CONFIRM_PASSWORD"  => $newPassword,
						                  "XML_ID" => $arMSAgent['externalCode'],
						                  'UF_CLOSE_CITY' => $idCity,
						                  "UF_ORGANIZATION" => $arMSAgent['legalTitle'] ? $arMSAgent['legalTitle'] : $arMSAgent['name'],
						                );

						                $idUserOrder = $connect->addNewCounterpartySite($arFieldsUser);

						                $arFieldsUpdateUser = array(
									      "USER_ID" => $idUserOrder,
									    );
									    print_r($arFieldsUser); 
									    CSaleOrder::Update($arOrderSite['ID'], $arFieldsUpdateUser);

				            		}
				            	}else{
				            		/* Обновление пользователя */

					                $arFieldsUserUpdate = Array(
					                  "XML_ID" => $arMSAgent['externalCode']
					                );

									$connect->addNewCounterpartySite($arFieldsUserUpdate);

			            			/* END */
				            	}

				            	/* Уже существует */

				                $currentStateID = $arOrderSite['STATUS_ID']; 
				                $idOrderSite = $arOrderSite['ID']; 

				                $dbBasket = \Bitrix\Sale\Basket::getList(array('filter' => array('ORDER_ID' => $idOrderSite)));
				                $goodsBasket = array();
				                while($bItem = $dbBasket->Fetch()){
				                     $goodsBasket[$bItem['ID']] = array(
				                          'QUANTITY' => (float)$bItem['QUANTITY'],
				                          'XML_ID' => $bItem['PRODUCT_XML_ID'],
				                          'PRICE' => (int)$bItem['PRICE'] * 100,
				                     );
				                }

				               
				                $posMS = array();
				                $posSite = array(); 

				                foreach ($arPositionsMS as $arGoodMS) {
				                	if($arGoodMS['XML_ID'] == 'deliveryorder'){ continue; }

				                	$posMS[$arGoodMS['XML_ID']] = array(
				                		"PRICE" => $arGoodMS['PRICE'],
				                		"QUANTITY" => $arGoodMS['QUANTITY']
				                	);
				                }
				                
				                foreach ($goodsBasket as $idBasket => $arGoodSite) {
				                	$posSite[$arGoodSite['XML_ID']] = array(
				                		"PRICE" => $arGoodSite['PRICE'], 
				                		"QUANTITY" => $arGoodSite['QUANTITY'],
				                		"ID_BASKET" => $idBasket
				                	);
				                }
				                
				                $arForAddItemToBasket = array();
				                $arForDeleteItemToBasket = array(); 

				                foreach ($posMS as $xml => $good) {
				                	if(!$posSite[$xml]){
										
										$dbItem = CIBlockElement::GetList(array(),
											array("IBLOCK_ID"=>22, "XML_ID"=>$xml),false,false,
											array("ID","NAME","IBLOCK_SECTION_ID"))->Fetch();

										if($dbItem['ID']){
											$arFieldsItem = array(
												"PRODUCT_ID" => $dbItem['ID'],
												"PRICE" => (float)$good['PRICE']/100,
												"CURRENCY" => "RUB",
												"QUANTITY" => $good['QUANTITY'],
												"PRODUCT_XML_ID" => $xml,
												"LID" => LANG,
												"DELAY" => "N",
												"CAN_BUY" => "Y",
												"ORDER_ID" => $idOrderSite,
												"NAME" => $dbItem['NAME'],
												"DETAIL_PAGE_URL" => "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=22&type=aspro_optimus_catalog&ID=".$dbItem['ID']."&lang=ru&find_section_section=".$dbItem['IBLOCK_SECTION_ID']."&WF=Y"
											);
											print_r($arFieldsItem); 
											CSaleBasket::Add($arFieldsItem);
										}

				                	}else{
				                		/*Обновление если товары несоответствуют */

				                		$idBasket = $posSite[$xml]['ID_BASKET'];
				                		$quantityItem = $posSite[$xml]['QUANTITY'];
				                		$priceItem = $posSite[$xml]['PRICE'] * 100;

				                		$arFieldsUpdate = array();

				                		if($quantityItem != $posMS[$xml]['QUANTITY']){
				                			$arFieldsUpdate['QUANTITY'] = $posMS[$xml]['QUANTITY'];
				                		}
				                		
				                		if($priceItem != $posMS[$xml]['PRICE']){
				                			$arFieldsUpdate['PRICE'] = $posMS[$xml]['PRICE']/100;
				                		}
				                		
				                		
				                		if(!empty($arFieldsUpdate)){
				                			CSaleBasket::Update($idBasket, $arFieldsUpdate);	
				                		}

				                		/* END Обновление если товары несоответствуют */
										

				                	}
				                }

				                foreach ($posSite as $xml => $good) {
				                	if(!$posMS[$xml]){ 
				                		CSaleBasket::Delete($good['ID_BASKET']); 
				                	}
				                }


				                if($dbStateSite['ID'] && ($currentStateID != $dbStateSite['ID'])){
									CSaleOrder::StatusOrder($idOrderSite, $lettersState);
				                }

				                /* Проверка оплат */
				                
				                $fullPriceOrder = $arMSOrder['sum'];
				                
				                if(!empty($arMSOrder['payments'])){
				                	$sumPricePayDocs = 0; 
				                	foreach ($arMSOrder['payments'] as $arPayment) {
				                		$sumPricePayDocs += $arPayment['linkedSum']; 
				                	}

				                	if($sumPricePayDocs >= $fullPriceOrder){
				                		MSOrdersExchange::checkPayedProp($idOrderSite, 'F');
				                	}elseif($sumPricePayDocs && ($sumPricePayDocs < $fullPriceOrder)){
				                		MSOrdersExchange::checkPayedProp($idOrderSite, 'P');
				                	}else{
				                		MSOrdersExchange::checkPayedProp($idOrderSite, 'N');
				                	}

				                }else{
				                	MSOrdersExchange::checkPayedProp($idOrderSite, 'N');
				                }

				                /* END Проверка оплат */
				                

				            }else{

				            	print_r('Заказ не существует'); 
				            	print_r($arMSOrderShort);

				                $newPassword = MSOrdersExchange::generatePassword(); 
				                
				                $name = $arMSOrderShort['USER']['name'];
				                $arParams = array("replace_space"=>"","replace_other"=>"");
				                $stubEmail = Cutil::translit($name,"ru",$arParams);
				                $stubEmail .= '@test.tx'; 

				                $arFieldsUser = Array(
				                  "NAME"              => $arMSOrderShort['USER']['name'],
				                  "EMAIL"             => $arMSOrderShort['USER']['email'] ? $arMSOrderShort['USER']['email'] : $stubEmail,
				                  "LOGIN"             => $arMSOrderShort['USER']['email'] ? $arMSOrderShort['USER']['email'] : $stubEmail,
				                  "LID"               => "s1",
				                  "ACTIVE"            => "Y",
				                  "GROUP_ID"          => array(2,3,6),
				                  "PASSWORD"          => $newPassword,
				                  "CONFIRM_PASSWORD"  => $newPassword,
				                  "XML_ID" => $arMSOrderShort['USER']['ext_code'],
				                  'UF_CLOSE_CITY' => $idCity,
				                  'UF_ORGANIZATION' => $arMSOrderShort['USER']['legalTitle'] ? $arMSOrderShort['USER']['legalTitle'] : $arMSOrderShort['USER']['name'],
				                );

				                // print_r($arFieldsUser); 

				                $idUserOrder = $connect->addNewCounterpartySite($arFieldsUser);


				                $priceOrder  = $arMSOrderShort['SUM']/100; 
				                
				                $arFieldsOrder = array(
				                   "LID" => "s1",
				                   "PERSON_TYPE_ID" => 1,
				                   "PAYED" => "N",
				                   "CANCELED" => "N",
				                   "STATUS_ID" => $dbStateSite['ID'] ? $dbStateSite['ID'] : "NO",
				                   "PRICE" => $priceOrder,
				                   "CURRENCY" => "RUB",
				                   "USER_ID" => $idUserOrder,
				                   "PAY_SYSTEM_ID" => 1,
				                   "PRICE_DELIVERY" => 0,
				                   // "DELIVERY_ID" => 1,
				                   "DISCOUNT_VALUE" => 0,
				                   "TAX_VALUE" => 0.0,
				                   "XML_ID" => $arMSOrderShort['XML_ID'],
				                   // "USER_DESCRIPTION" => "",
				                );

				                if(!empty($arMSOrderShort['BASKET'])){
				                    $arItemsForAdd = array(); 
				                    foreach ($arMSOrderShort['BASKET'] as $arItemBasket) {
				                        $arItemsForAdd[] = array(
				                            "PRICE" => $arItemBasket['PRICE']/100,
				                            "XML_ID" => $arItemBasket['XML_ID'],
				                            "QUANTITY" => $arItemBasket['QUANTITY']
				                        );
				                    }
				                }

				                print_r($arFieldsOrder);
				                print_r($arItemsForAdd);
				                print_r($locationCity);
				                $idNewOrder = MSOrdersExchange::addNewOrderSite($arFieldsOrder, $arItemsForAdd, $locationCity);
				                if($idNewOrder){
				                    // $resultUpdateCode = $connect->updateOrderExtCode($arMSOrder, $idNewOrder); 
				                    unset($connect);
				                    
				                    $connect = new MSOrdersExchange($loginCity, $passCity);
				                    $connect->addIDOrderSiteToMS($idNewOrder, $arMSOrder);
				                    
				                    /* Отправка почты */

				                    $dbNewOrder = CSaleOrder::GetByID($idNewOrder); 

								    $dbBasket = \Bitrix\Sale\Basket::getList(array('filter' => array('ORDER_ID' => $idNewOrder)));
								    
								    $lineBasket = '';

								    while($bItem = $dbBasket->Fetch()){
								        $priceItemFormat = number_format($bItem['PRICE'], 0,"."," ").' руб.';
								        $formatQuantity = number_format($bItem['QUANTITY'], 0,"."," ");
								        $lineBasket .= $bItem['NAME'].' - '.$formatQuantity.' шт x '.$priceItemFormat.'<br />';
								    }

								    $priceFormat = number_format($dbNewOrder['PRICE'], 0,"."," ").' руб.';

								    $arEventFields = array(
								        'ORDER_ID' => $idNewOrder,
								        'ORDER_REAL_ID' => $idNewOrder,
								        'ORDER_ACCOUNT_NUMBER_ENCODE' => $idNewOrder,
								        'ORDER_DATE' => $dbNewOrder['DATE_INSERT'],
								        'ORDER_USER' => $dbNewOrder['USER_LAST_NAME'],
								        'PRICE' => $priceFormat,
								        'BCC' => 'mail@memstech.ru',
								        'EMAIL' => $dbNewOrder['USER_EMAIL'],
								        'ORDER_LIST' => $lineBasket,
								        'SALE_EMAIL' => 'mail@memstech.ru',
								        'DELIVERY_PRICE' => $dbNewOrder['PRICE_DELIVERY'],
								        'ORDER_PUBLIC_URL' => ''
								    );


								    $event = new CEvent;
								    $event->SendImmediate("SALE_NEW_ORDER", 's1', $arEventFields);

				                    /* END Отправка почты */

				                    unset($connect); 
				                }

				           		/* Проверка оплат */
				                
				                $fullPriceOrder = $arMSOrder['sum'];
				                
				                if(!empty($arMSOrder['payments'])){
				                	$sumPricePayDocs = 0; 
				                	foreach ($arMSOrder['payments'] as $arPayment) {
				                		$sumPricePayDocs += $arPayment['linkedSum']; 
				                	}

				                	if($sumPricePayDocs >= $fullPriceOrder){
				                		MSOrdersExchange::checkPayedProp($idNewOrder, 'F');
				                	}elseif($sumPricePayDocs && ($sumPricePayDocs < $fullPriceOrder)){
				                		MSOrdersExchange::checkPayedProp($idNewOrder, 'P');
				                	}else{
				                		MSOrdersExchange::checkPayedProp($idNewOrder, 'N');
				                	}

				                }else{
				                	MSOrdersExchange::checkPayedProp($idNewOrder, 'N');
				                }

				                /* END Проверка оплат */

				            }
				        }
				    }else{
				    	$objWorkLog->addNewLogLine('В МС '.$loginCity.' нет новых заказов для загрузки на сайт');
				    }

				    unset($objConnectOrder); 

				}


			/******************* END МС -> Сайт ***************************************/



			MSETools::setFlag('block_order',"N");
		}else{
			$currentCntOrderBlock = MSETools::getFlag('cnt_block_orders'); 
			if($currentCntOrderBlock){
				MSETools::setFlag('cnt_block_orders', ++$currentCntOrderBlock);  
			}else{
				MSETools::setFlag('cnt_block_orders', 1);  
			}
		}

		/* END Заказы */


	}

	// header( "refresh:2; url=/bitrix/admin/ms_json_cron.php" );

	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php" ) ;


?>