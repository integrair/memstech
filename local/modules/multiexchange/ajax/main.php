<? 
define("NOT_CHECK_PERMISSIONS",true); 
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule('iblock'); 
\Bitrix\Main\Loader::includeModule('multiexchange');
$arOut = array();

if($_REQUEST['action'] == 'create_city'){
	$arFieldsCity = array(
	    "NAME" => $_REQUEST['name'],
	    "LOGIN" => $_REQUEST['login'],
	    "PASSWORD" => $_REQUEST['password'],
	    "IBLOCK_ID" => 18,
	    "CODE" => $_REQUEST['code']
	);
	
	$newCity = new MECitys($arFieldsCity); 
	if(!empty($newCity->messages['ERROR'])){
		$arOut['ERROR_PRINT'] = $newCity->messages['ERROR']; 
	}else{
		if($newCity->dataChecking()){
		 	$newCity->addCity();
		 	if($newCity->idWorkElement){
		 		$arOut['SUCCESS'] = 'Y';
		 	}
	    }else{
	    	if(!empty($newCity->messages['ERROR'])){
	    		$arOut['ERROR_PRINT'] = $newCity->messages['ERROR'];
	    	}
	    }
	}
}elseif(($_REQUEST['action'] == 'test_connect') && $_REQUEST['id']){
	$idCity = $_REQUEST['id'];
	$dbCity = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>18,"ID"=>$idCity),false,false,array("ID","PROPERTY_LOGIN","PROPERTY_PASSWORD"))->Fetch();
	if($dbCity['ID']){
		
		$login = $dbCity['PROPERTY_LOGIN_VALUE'];
		$password = $dbCity['PROPERTY_PASSWORD_VALUE'];
		$objConnect = new MSExchange($login, $password, 22);
		$arOut['result'] = $objConnect->testConnect();	

	}else{
		$arOut['error'] = 'Y'; 
	}
	

}elseif(($_REQUEST['action'] == 'change_city') && $_REQUEST['id']){
	
	$idCity = $_REQUEST['id'];
	$login = $_REQUEST['login'];
	$password = $_REQUEST['password'];
	$checkSynch = $_REQUEST['check_synch'];

	if($checkSynch == 'true'){
		CIBlockElement::SetPropertyValuesEx($idCity, 18,array("CHECK_SYNCH"=>30));
	}else{
		CIBlockElement::SetPropertyValuesEx($idCity, 18,array("CHECK_SYNCH"=>false));
	}

	if($login){
		CIBlockElement::SetPropertyValuesEx($idCity, 18,array("LOGIN"=>$login));
	}

	if($password){
		CIBlockElement::SetPropertyValuesEx($idCity, 18,array("PASSWORD"=>$password));
	}

}elseif(($_REQUEST['action'] == 'delete-city') && $_REQUEST['id']){
	$idCity = $_REQUEST['id']; 
	if(CIBlockElement::Delete($idCity)){
		$arOut['result'] = 'true'; 
	}else{
		$arOut['result'] = 'false'; 
	}

}elseif($_REQUEST['action'] == 'unload-items'){

}elseif($_REQUEST['action'] == 'get-list-flags'){
	ob_start(); ?>
	<table>
	<?if(file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/EXCHANGE')):
		
		$arContentEXCHANGE = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/EXCHANGE'));
		foreach ($arContentEXCHANGE as $keyFlag => $valueFlag):?>
		<tr>
			<td><?=$keyFlag; ?></td>
			<? if(is_array($valueFlag)):?>
				<td><pre><? print_r($valueFlag); ?></pre></td>
			<? else:?>
				<td><?=$valueFlag; ?></td>
			<? endif;?>

			
		</tr>	
		<? endforeach;
	endif; 

	if(file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/ORDERS')):
		$arContentORDERS = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/ORDERS'));?>
		<tr>
			<td>Заказы</td>
			<td><? print_r($arContentORDERS); ?></td>
		</tr>	
		
	<? endif; ?>

	</table>
	<? $arOut['html_result'] = ob_get_clean();

}elseif(($_REQUEST['action'] == 'bind_prices') && $_REQUEST['id_city']){
	
	$idCity = $_REQUEST['id_city'];
    $dbCity = CIBlockElement::GetList(
        array(),
        array("IBLOCK_ID"=>18, "ID"=>$idCity),false,false,
        array(
            "PROPERTY_LOGIN",
            "PROPERTY_PASSWORD",
            "PROPERTY_MS_BINDS_PRICES")
    )->Fetch();

    $password = $dbCity['PROPERTY_PASSWORD_VALUE'];
    $login = $dbCity['PROPERTY_LOGIN_VALUE'];
    $pricesBinds = $dbCity['PROPERTY_MS_BINDS_PRICES_VALUE'];

    /* Типы цен из МС */
    $exchangeOBJ = new MSExchange($login, $password, 22);
    $resultMetaItems = $exchangeOBJ->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/product/metadata');
    $typePricesMS = $resultMetaItems['priceTypes'];
    // $testPriceFromMS = array();
    // foreach ($typePricesMS as $price_ms){
    // 	$testPriceFromMS[] = $price_ms['name']; 
    // }

    /* Типы цен сайта*/
    $dbGroup = CCatalogGroup::GetList(array(),array(),false,false,array("ID","NAME","XML_ID"));
    $arPricesSite = array(); 
    // $testPriceFromSite = array();
    while ($rtp = $dbGroup->Fetch()) {
        $arPricesSite[] = $rtp;
        // $testPriceFromSite[] = $rtp['XML_ID']; 
    }


    if($_REQUEST['type'] == 'add_select'){

	    ob_start(); ?>

			<div class="bind-line">
				<select name="select-site-prices[]" class="select-site-prices">
					<option value="">--пусто--</option>
					<? foreach ($arPricesSite as $itemTypeSite):?>
						<option value="<?=$itemTypeSite['XML_ID'];?>"><?=$itemTypeSite['NAME']; ?></option>
					<?endforeach;?>

				</select>
				<select name="select-ms-prices[]" class="select-ms-prices">
					<option value="">--пусто--</option>
					<? foreach($typePricesMS as $itemTypeMS):?>
						<option value="<?=$itemTypeMS['name'];?>"><?=$itemTypeMS['name'];?></option>
					<? endforeach;?>
				</select>
			</div>

		<? $arOut['html_result'] = ob_get_clean();
    
    }else{

		ob_start(); ?>
			
				<div class="wrap-binds-lines">
					<div class="header-binds-table">
						<span class="h-title" style="float:left">Цена на сайте</span>
						<span class="h-title">Цена в МС</span>
					</div>
					<?if($pricesBinds):?>
    					<? $arPricesDB = unserialize(base64_decode($pricesBinds));
    					?>

						<? foreach ($arPricesDB as $arBindDB):?>
							
							<div class="bind-line">
								<select name="select-site-prices[]" class="select-site-prices">
									<option value="">--пусто--</option>
									<? foreach ($arPricesSite as $itemTypeSite):?>
										<option value="<?=$itemTypeSite['XML_ID'];?>" <?if($arBindDB['site'] == $itemTypeSite['XML_ID']):?>selected<?endif;?>><?=$itemTypeSite['NAME']; ?></option>
									<?endforeach;?>

								</select>
								<select name="select-ms-prices[]" class="select-ms-prices">
									<option value="">--пусто--</option>
									<? foreach($typePricesMS as $itemTypeMS):?>
										<option value="<?=$itemTypeMS['name'];?>" <?if($arBindDB['ms'] == $itemTypeMS['name']):?>selected<?endif;?>><?=$itemTypeMS['name'];?></option>
									<? endforeach;?>
								</select>
							</div>

						<? endforeach; ?>
    				<? else:?>
						<div class="bind-line">
							<select name="select-site-prices[]" class="select-site-prices">
								<option value="">--пусто--</option>
								<? foreach ($arPricesSite as $itemTypeSite):?>
									<option value="<?=$itemTypeSite['XML_ID'];?>"><?=$itemTypeSite['NAME']; ?></option>
								<?endforeach;?>

							</select>
							<select name="select-ms-prices[]" class="select-ms-prices">
								<option value="">--пусто--</option>
								<? foreach($typePricesMS as $itemTypeMS):?>
									<option value="<?=$itemTypeMS['name'];?>"><?=$itemTypeMS['name'];?></option>
								<? endforeach;?>
							</select>
						</div>
    				<? endif;?>
						
				</div>
			
			<p style="text-align:center;">
				<a href="#" class="add-new-prices-bind small-window-link" city="<?=$_REQUEST['id_city']; ?>">Добавить привязку</a>
			</p>
			<p style="text-align:center;">
				<a href="#" id="save-prices-binds" class="small-window-link" city="<?=$_REQUEST['id_city']; ?>">Сохранить привязки</a>
			</p>
			<p class='messsage-line-prices'></p>




		<? $arOut['html_result'] = ob_get_clean();

	}
}elseif(($_REQUEST['action'] == 'bind_stores') && $_REQUEST['id_city']){
	
	$idCity = $_REQUEST['id_city'];
    $dbCity = CIBlockElement::GetList(
        array(),
        array("IBLOCK_ID"=>18, "ID"=>$idCity),false,false,
        array(
            "PROPERTY_LOGIN",
            "PROPERTY_PASSWORD",
            "PROPERTY_MS_BINDS_STORES")
    )->Fetch();

    $password = $dbCity['PROPERTY_PASSWORD_VALUE'];
    $login = $dbCity['PROPERTY_LOGIN_VALUE'];
    $storesBinds = $dbCity['PROPERTY_MS_BINDS_STORES_VALUE'];

    /* Склады из МС */
    $exchangeOBJ = new MSExchange($login, $password, 22);
    $resultMetaItems = $exchangeOBJ->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/store');
    $reqStoresMS = $resultMetaItems['rows'];
    $listStoresMS = array(); 
    if(!empty($reqStoresMS)){
	    foreach ($reqStoresMS as $msStoreDesc){
	    	$listStoresMS[$msStoreDesc['id']] = $msStoreDesc['name'];
	    }	
    }

    /* Склады с сайта*/
    
    $dbStores = CCatalogStore::GetList(array(),array(),false,false,array("ID","TITLE"));
    $arStoresSite = array(); 
    while($rs = $dbStores->Fetch()){
        $arStoresSite[$rs['ID']] = $rs['TITLE']; 
    }
		ob_start(); ?>
			<? $arBindsStoresDB = array(); 
			   if($storesBinds){
			   		$arBindsStoresDB = unserialize(base64_decode($storesBinds));
			   }?>

			<div class="wrap-binds-lines-stores">
				<div class="header-binds-table">
					<span class="h-title" style="float:left">Склад на сайте</span>
					<span class="h-title">Склад в МС</span>
				</div>
				<div class="bind-line-store">
					<select name="select-site-stores[]" class="select-site-stores">
						<option value="">--пусто--</option>
						<? foreach ($arStoresSite as $idStoreSite => $nameStoreSite):?>
							<option value="<?=$idStoreSite;?>" <?if($arBindsStoresDB['site'] == $idStoreSite):?>selected<?endif;?>><?=$nameStoreSite; ?></option>
						<?endforeach;?>

					</select>
					<select name="select-ms-stores[]" class="select-ms-stores">
						<option value="">--пусто--</option>
						<? foreach($listStoresMS as $idStoreMS => $nameStore):?>
							<option value="<?=$idStoreMS;?>" <?if($arBindsStoresDB['ms'] == $idStoreMS):?>selected<?endif;?>><?=$nameStore;?></option>
						<? endforeach;?>
					</select>
				</div>
			</div>
			<p style="text-align:center; ">
				<a href="#" id="save-stores-binds" class="small-window-link" city="<?=$_REQUEST['id_city']; ?>">Сохранить привязки</a>
			</p>
			<p class='messsage-line-stores'></p>




		<? $arOut['html_result'] = ob_get_clean();

}elseif(($_REQUEST['action'] == 'save_binds_prices') && $_REQUEST['id_city']){
	
	$arOut['prices'] = $_REQUEST['prices']; 

	if(!empty($_REQUEST['prices'])){
		
		$arSavePrices = array(); 
		foreach ($_REQUEST['prices'] as $arBind) {
			if(!empty($arBind['site']) && !empty($arBind['ms'])){
				$arSavePrices[] = $arBind;	
			}			
		}

		$arOut['savear'] = $arSavePrices; 
		
		if(!empty($arSavePrices)){
			$lineArSavePrices = base64_encode(serialize($arSavePrices)); 
			CIBlockElement::SetPropertyValuesEx($_REQUEST['id_city'], 18,array("MS_BINDS_PRICES"=>$lineArSavePrices));	
		}else{
			CIBlockElement::SetPropertyValuesEx($_REQUEST['id_city'], 18,array("MS_BINDS_PRICES"=>false));
		}
		
	}else{
		CIBlockElement::SetPropertyValuesEx($_REQUEST['id_city'], 18,array("MS_BINDS_PRICES"=>false));
	}
	$arOut['confirm'] = 'Y'; 

}elseif(($_REQUEST['action'] == 'save_binds_stores') && $_REQUEST['id_city']){
	if(!empty($_REQUEST['stores'])){
		$arOut['req'] = $_REQUEST['stores']; 
		
		$arSaveStores = array(); 
		
		if($_REQUEST['stores']['site'] && $_REQUEST['stores']['ms']){
			$arSaveStores = $_REQUEST['stores']; 
		}

		if(!empty($arSaveStores)){
			$lineArSaveStores = base64_encode(serialize($arSaveStores)); 
			CIBlockElement::SetPropertyValuesEx($_REQUEST['id_city'], 18,array("MS_BINDS_STORES"=>$lineArSaveStores));	
		}else{
			CIBlockElement::SetPropertyValuesEx($_REQUEST['id_city'], 18,array("MS_BINDS_STORES"=>false));
		}
		
	}else{
		CIBlockElement::SetPropertyValuesEx($_REQUEST['id_city'], 18,array("MS_BINDS_STORES"=>false));
	}
	$arOut['confirm'] = 'Y';
}



echo json_encode($arOut); ?>