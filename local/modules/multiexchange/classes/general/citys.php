<?
class MECitys{
	public $login;
	public $password;
	public $name; 
	public $code; 
	public $idWorkElement;
	public $iblockCityID;
	public $messages; 

	function __construct($arParams){

		if(!$arParams['IBLOCK_ID']){ return false; }
		$this->iblockCityID = $arParams['IBLOCK_ID']; 
		if($arParams['IBLOCK_ELEMENT_ID']){
			$this->idWorkElement = $arParams['IBLOCK_ELEMENT_ID']; 
			$dbCity = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "ID"=>$arParams['IBLOCK_ELEMENT_ID']),false, false,array("ID","NAME","PROPERTY_LOGIN","PROPERTY_PASSWORD"))->Fetch(); 
			
			$this->name = $dbCity['NAME'];
			$this->login = ($dbCity['PROPERTY_LOGIN_VALUE'] ? $dbCity['PROPERTY_LOGIN_VALUE'] : '');
			$this->password = ($dbCity['PROPERTY_PASSWORD_VALUE'] ? $dbCity['PROPERTY_PASSWORD_VALUE'] : '');

		}elseif($arParams['NAME'] && $arParams['CODE']){
			$this->name = $arParams['NAME'];
			$this->code = $arParams['CODE']; 
			$this->login = ($arParams['LOGIN'] ? $arParams['LOGIN'] : '');
			$this->password = ($arParams['PASSWORD'] ? $arParams['PASSWORD'] : '');
		}else{
			$this->messages['ERROR'][] = 'Не указано название нового города или символьный код';

		}
		
	}

	function dataChecking(){
		if($this->idWorkElement){ return false; }
		$dbTestElement = CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID"=>$this->iblockCityID,
				  "NAME"=>$this->name),false, false,array("ID")
		)->Fetch(); 
		if($dbTestElement['ID']){
			$this->messages['ERROR'][] = 'Город с таким названием уже существует.';
			return false;
		}
		$dbTestElement = CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID"=>$this->iblockCityID,
				  "CODE"=>$this->code),false, false,array("ID")
		)->Fetch(); 
		if($dbTestElement['ID']){
			$this->messages['ERROR'][] = 'Такой символьный код уже используется';
			return false;
		}
		
		if($this->login){
			$dbTestElement = CIBlockElement::GetList(
				array(),
				array("IBLOCK_ID"=>$this->iblockCityID,
					  "PROPERTY_LOGIN"=>$this->login),false, false,array("ID")
			)->Fetch(); 
			if($dbTestElement['ID']){
				$this->messages['ERROR'][] = 'Такая учетная запись уже используется другим городом';
				return false;
			}				
		}

		return true; 


	}

	function testConnectMS(){

	}
	
	function addCity(){
		if($this->idWorkElement){ return false; }
		$el = new CIBlockElement;
		$arFieldAdd = array(
			"IBLOCK_ID" => $this->iblockCityID,
			"NAME" => $this->name,
			"ACTIVE" => 'Y',
			'CODE' => $this->code, 
			"PROPERTY_VALUES" => array(
				'102' => $this->login,
				'103' => $this->password
			)
		);

		if($this->idWorkElement = $el->Add($arFieldAdd)){

		}

	}
	
	function updateCitys(){

	}

	function deleteCity(){

	}



}
?>