<? 

class MSCounterItemCity{

	const SQLite_DB_PATH = '/bitrix/ms_flags/COUNTER.db';
	public $counter; 

	public function __construct(){
		$this->counter = new SQLite3($_SERVER['DOCUMENT_ROOT'].self::SQLite_DB_PATH);
        $queryLineNewTable = "CREATE TABLE CHECKITEMS(
            ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            ITEM INTEGER NOT NULL,
            CITY TEXT
        )";
        $this->counter->querySingle($queryLineNewTable);
        // print_r($this->counter->lastErrorMsg());
	}

	public function sqlite_addNewItemCity($idItem, $codeCity){
		$lineQuery = "INSERT INTO CHECKITEMS (ITEM, CITY) VALUES ($idItem, '$codeCity')";
		$this->counter->querySingle($lineQuery);
		// print_r($this->counter->lastErrorMsg());
    }

    public function sqlite_checkItemCity($idItem, $codeCity){
    	$lineQuery = "SELECT ID FROM CHECKITEMS WHERE ITEM = $idItem AND CITY = '$codeCity'";
    	$result = $this->counter->querySingle($lineQuery);
    	if($result) return true; 
    	return false;
    }

    public function sqlity_printCounterBase(){
        $lineQuery ="SELECT * FROM CHECKITEMS";

        $result = $this->counter->query($lineQuery);
        while ($row = $result->fetchArray()) {
		    print_r($row);
		}
    }

    public function destroyCounter(){
    	$this->counter->close();
    	unlink($_SERVER['DOCUMENT_ROOT'].self::SQLite_DB_PATH);
    }
	
}

?>