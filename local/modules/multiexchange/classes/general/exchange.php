<?
use Bitrix\Main\Web\HttpClient;
use Bitrix\Iblock\ElementTable;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

CModule::IncludeModule("highloadblock");


class MSExchange{
	
	private $httpConnect;
	public $bindExtCodesToIDMS; 
	public $idIBlock;
	public $arSectionSiteInfo;
	public $arSectionMSInfo; 

	public static $arSectionMSInfoST = array(); 
	public static $bindExtCodesToIDMSST = array(); 
	public static $arControlUpdate; 

	public static $counterStocksLinks;
	public static $arFinalStocksBinds = array(); 
	public static $nextLinkStocks; 

	public function __construct($login, $password, $idCatalog){
		if($idCatalog){
			$this->idIBlock = $idCatalog; 	
		}
		
		$this->httpConnect = new HttpClient(); 
	    $this->httpConnect->setHeader('Content-Type', 'application/json; charset=utf-8', true); 
	    $this->httpConnect->setAuthorization($login, $password);
	}
	
	public function simpleGetToURL($url){
		return $this->httpConnect->get($url); 
	}


	public function getRequest($url){
		$request = $this->httpConnect->get($url); 
		$temp = json_decode($request, true);
		return $temp;
	}

	public function postRequest($url, $arBody){
      $temp = json_decode(
        $this->httpConnect->post($url, json_encode($arBody)), true
      );
      return $temp; 
	}
	
	public function putRequest($url, $arBody){
      $temp = json_decode(
        $this->httpConnect->query('PUT', $url, json_encode($arBody)), true
      );
      return $temp; 
	}

	public function testConnect(){
		$request = $this->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/metadata'); 
		return $request; 
	}
	
	public function createBindsCodesArray($nextHref, $codeCity){

		if(!$nextHref){
			$request = $this->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/productfolder?limit=100');	
		}else{
			$request = $this->getRequest($nextHref);	
		}
		
		if(!empty($request['rows'])){
			foreach ($request['rows'] as $arMsSection) {

				self::$bindExtCodesToIDMSST[$codeCity][$arMsSection['externalCode']] = $arMsSection['id'];
				
				if($arMsSection['productFolder']['meta']['href']){
					$arExp = explode('/', $arMsSection['productFolder']['meta']['href']);
					$idParentSection = $arExp[count($arExp) - 1]; 
				}

				self::$arSectionMSInfoST[$codeCity][$arMsSection['externalCode']] = array(
					"ID"=>$arMsSection['id'], 
					'NAME'=>$arMsSection['name'], 
					"FOLDER" => $arMsSection,
					"PARENT_LINK"=>$arMsSection['productFolder'],
					'ID_PARENT_MS' => $idParentSection
				);

				unset($idParentSection);

			}
			
			foreach (self::$arSectionMSInfoST[$codeCity] as &$arFolder) {
				if($arFolder['ID_PARENT_MS']){
					$arFolder['EXT_CODE_PARENT'] = array_search($arFolder['ID_PARENT_MS'], self::$bindExtCodesToIDMSST[$codeCity]); 
				}
			}
		}

		if($request['meta']['nextHref']){
			$this->createBindsCodesArray($request['meta']['nextHref'], $codeCity); 
		}

		$this->bindExtCodesToIDMS = self::$bindExtCodesToIDMSST[$codeCity];
		$this->arSectionMSInfo = self::$arSectionMSInfoST[$codeCity];

	}

	public function createSectionMS($arFields){
		$arBody = array();
		$arBody['name'] = $arFields['name'];
		$arBody['externalCode'] = $arFields['externalCode'];
		
		if(!empty($arFields['productFolder'])){
			$arBody['productFolder'] = $arFields['productFolder']; 
		}

		$urlCreate = 'https://online.moysklad.ru/api/remap/1.1/entity/productfolder';
		$result = $this->postRequest($urlCreate, $arBody); 
		return $result;
	}

	public function changeSectionMS($arFields){

	}

	public function getInfoSectionSite(){
		$this->arSectionSiteInfo = array();
		$dbSection = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>$this->idIBlock/*,"ACTIVE"=>"Y"*/),false,array("ID","XML_ID","NAME","IBLOCK_SECTION_ID"));
		$arBindsXMLID = array();

		while($rs = $dbSection->Fetch()){
			if($rs['XML_ID']){
				$arBindsXMLID[$rs['ID']] = $rs['XML_ID'];
				$this->arSectionSiteInfo[$rs['XML_ID']] = array(
					"NAME"=>$rs['NAME'],
					"ID"=>$rs['ID'],
					"PARENT"=>$rs["IBLOCK_SECTION_ID"]
				);

			}
		}
		
		foreach ($this->arSectionSiteInfo as &$arSection){
			if($arBindsXMLID[$arSection["PARENT"]]){
				$arSection['PARENT_XML_ID'] = $arBindsXMLID[$arSection["PARENT"]];	
			}
			
		}
	}

	public static function recursiveUpdateSection($arWork, $isRoot, $login, $password, $arExistsSections){
		$arNext = array(); 

		foreach ($arWork as $extCode => $arSectSite){
			if(!empty($arExistsSections[$extCode])){
				

				self::$arControlUpdate[$login][$extCode] = $arExistsSections[$extCode]['FOLDER']; 
			 	continue; 
			 }
		

			if($isRoot && !$arSectSite['PARENT_XML_ID']){
				
				
				$arFields = array(
					'name' => $arSectSite['NAME'],
					'externalCode' => $extCode,
				);

				$objConnect = new static($login, $password, 22);
			   	
			    $resultAdd = $objConnect->createSectionMS($arFields);


			    unset($arFields);
			    unset($objConnect);

			    self::$arControlUpdate[$login][$extCode] = $resultAdd; 
			}elseif($arMSFolder = self::$arControlUpdate[$login][$arSectSite['PARENT_XML_ID']]){
				
				$arFields = array(
					'name' => $arSectSite['NAME'],
					'externalCode' => $extCode,
					'productFolder' => array('meta' => $arMSFolder['meta'])
				);

				$objConnect = new static($login, $password, 22);
			    $resultAdd = $objConnect->createSectionMS($arFields);


			    unset($arFields);
			    unset($objConnect);
			    self::$arControlUpdate[$login][$extCode] = $resultAdd;

			}else{
				if(!in_array($extCode, self::$arControlUpdate[$login])){
					$arNext[$extCode] = $arSectSite; 	
				}
			}
		}
		
		if(!empty($arNext)){
			self::recursiveUpdateSection($arNext, false, $login, $password, $arExistsSections); 
		}

	}

	public function isItemsExistInMS($externalCode){

		$filterLine = urlencode('externalCode='.$externalCode);

		$request = $this->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/product?filter='.$filterLine); 

		if(!empty($request['rows'])){
			return $request['rows'][0]['id'];
		}

		return false; 
	}
	
	public function getItemsInfoMS($externalCode){

		$filterLine = urlencode('externalCode='.$externalCode);

		$request = $this->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/product?filter='.$filterLine); 
		if(!empty($request['rows'])){
			return $request['rows'][0];
		}

		return false; 
	}	

	public function getArMainCurrency($isoCode){
		if($isoCode){
			$reqCurrency = $this->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/currency/?filter='.urlencode('isoCode='.$isoCode)); 	
		}else{
			$reqCurrency = $this->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/currency/'); 
		}

		if(!empty($reqCurrency['rows'])){
			return $reqCurrency['rows'][0]['meta'];
		}
		return false; 
	}

	public function getStatesList(){
		$temp = $this->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/customerorder/metadata/');
		
		$arStatesForWork = array();

		if(!empty($temp['states'])){
			foreach ($temp['states'] as $arStateMS) {
				
				unset($arStateMS['meta']);
				unset($arStateMS['id']);
				unset($arStateMS['accountId']);
				$arStatesForWork[] = $arStateMS;
				
			}
			
		}
		return $arStatesForWork; 
	}

	public function getLastUdatedOrdersMS($backTime){

		$backTime = (int)$backTime; 
		$exceptionStatusUpdateSite = array('N','R');

		$date_last = date('Y-m-d%20H:i:s', mktime(date("H"), 
			date("i")-$backTime, date("s"), date("m"),
			date("d"), date("Y")));

		$tempOrders = json_decode($this->httpConnect->get('https://online.moysklad.ru/api/remap/1.1/entity/customerorder?filter=updated%3E' . $date_last), true);

		return $tempOrders; 
	}

	public function getAllTypesPriceMS(){
		$resultMetaItems = $this->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/product/metadata');
        $resultTypeNames = array();
        if(!empty($resultMetaItems['priceTypes'])){
        	foreach ($resultMetaItems['priceTypes'] as $priceMSName) {
        		$resultTypeNames[] = $priceMSName['name']; 
        	}
        }
        return $resultTypeNames; 
	}

	public function getRecursiveAllStocks($url, $xmlCityStore, $step){
		
		if(!self::$counterStocksLinks){
			self::$counterStocksLinks = $step; 	
		}

		if(!$url){
			 $url = 'https://online.moysklad.ru/api/remap/1.1/report/stock/bystore?limit=100';
		}

		$targetStoreXML = 'https://online.moysklad.ru/api/remap/1.1/entity/store/'.$xmlCityStore; 

		$resultStores = $this->getRequest($url); 

		$nextUrl = $resultStores['meta']['nextHref']; 
		self::$nextLinkStocks = $resultStores['meta']['nextHref'];
		
		print_r($targetStoreXML); 
		print_r($resultStores); 

		foreach ($resultStores['rows'] as $arStoresItem) {
			
			$idXMLItem = str_replace('?expand=supplier', '', str_replace('https://online.moysklad.ru/api/remap/1.1/entity/product/', '', $arStoresItem['meta']['href']));

			// $idXMLItem = str_replace('https://online.moysklad.ru/app/#good/edit?id=', '', $arStoresItem['meta']['uuidHref']);
			
			print_r($idXMLItem);
			print_r('<br />');

			foreach ($arStoresItem['stockByStore'] as $arStocksStore) {
				if($arStocksStore['meta']['href'] == $targetStoreXML){
					self::$arFinalStocksBinds[$idXMLItem] = $arStocksStore['stock']; 
				}
			}
		}



		self::$counterStocksLinks--; 

		if((self::$counterStocksLinks > 0) && $nextUrl){
			$this->getRecursiveAllStocks($nextUrl, $xmlCityStore, $step);
		}

	}

	public static function writeNewItemCityToTDB($xml_id, $city_id, $amount){
        if(!$xml_id || !$city_id){ return false; }

        $hlblock = HL\HighloadBlockTable::getById(3)->fetch(); 
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);  
        $entity_data_class = $entity->getDataClass(); 

        $arFilter = array(
            "UF_XML_ID_CITY" => $xml_id,
            "UF_CITY" => $city_id,
        );
        
        $arSelect = array("UF_XML_ID_CITY","UF_AMOUNT_STORE","UF_CITY","ID");

        $rsData = $entity_data_class::getList(array(
            "select" => $arSelect, 
            "filter" => $arFilter,
            "order" => array() 
        ))->fetch();

        if($rsData['ID']){

            if($rsData['UF_AMOUNT_STORE'] != $amount){

                $arFields = array(
                    "UF_XML_ID_CITY" => $xml_id,
                    "UF_AMOUNT_STORE" => $amount, 
                    "UF_CITY" => $city_id,
                );

                $entity_data_class::update($rsData['ID'], $arFields);
            }
        }else{

            $arFields = array(
                "UF_XML_ID_CITY" => $xml_id,
                "UF_AMOUNT_STORE" => $amount, 
                "UF_CITY" => $city_id,
            );

            $entity_data_class::add($arFields);
        }
    }

    public static function deleteItemCityTDB($xml_id, $city_id){

        $hlblock = HL\HighloadBlockTable::getById(3)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $arFilter = array(
            "UF_XML_ID_CITY" => $xml_id,
            "UF_CITY" => $city_id,
        );
        
        $arSelect = array("UF_XML_ID_CITY","UF_AMOUNT_STORE","UF_CITY","ID");

        $rsData = $entity_data_class::getList(array(
            "select" => $arSelect, 
            "filter" => $arFilter,
            "order" => array() 
        ))->fetch();

        if($rsData['ID']){
            $entity_data_class::delete($rsData['ID']);    
        }

    }

    public static function getAmountCityToXML_ID($xml_id, $city_id){

        $hlblock = HL\HighloadBlockTable::getById(3)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $arFilter = array(
            "UF_XML_ID_CITY" => $xml_id,
            "UF_CITY" => $city_id,
        );
        
        $arSelect = array("UF_AMOUNT_STORE","ID");

        $rsData = $entity_data_class::getList(array(
            "select" => $arSelect, 
            "filter" => $arFilter,
            "order" => array() 
        ))->fetch();
        
        if($rsData['ID']){
            return $rsData['UF_AMOUNT_STORE']; 
        }else{
            return 0; 
        }
        
    }

    public static function resetAmountToStoreProduct($id_site_product, $id_store){
        if(!$id_site_product || !$id_store){
            return false;
        }else{ 
            $arStoreProduct = CCatalogStoreProduct::GetList(array(), array("STORE_ID" => $id_store, "PRODUCT_ID"=>$id_site_product))->Fetch();
            if($arStoreProduct['ID']){
                return CCatalogStoreProduct::Update($arStoreProduct['ID'], array('AMOUNT' => 0));
            }else{
                return false;
            }
        }
    }

    public static function updateAmountStoreProduct($idProduct, $idStore, $amount){
        if($idProduct && $idStore && $amount){
        	 $arStoreAmount =CCatalogStoreProduct::GetList(
        	 	array(),array("PRODUCT_ID" => $idProduct, "STORE_ID" => $idStore),false,false,array("ID"))->Fetch();
        	
        	$arFields = Array(
                "PRODUCT_ID" => $idProduct,
                "STORE_ID" => $idStore,
                "AMOUNT" => $amount,
            );

			if($arStoreAmount['ID']){
				$ID = CCatalogStoreProduct::Update($arStoreAmount['ID'], $arFields); 
			}else{
				$ID = CCatalogStoreProduct::Add($arFields);
			}

        }

    }




}
?>