<?

use Bitrix\Main\Web\HttpClient;
use Bitrix\Iblock\ElementTable;

class MSEItems{
	
	public static $arSectionSiteInfo; 
	public static $arSiteItems; 
	public $arMainCurrency;
	public $arAltCurrency; 
	public $bodyItemMS;

	public function __construct($arFields, $arSectionMSInfo){

		$this->bodyItemMS = array(); 
		$this->bodyItemMS['name'] = $arFields['NAME'];
		
		if($arFields['ARTICUL']){
			$this->bodyItemMS['article'] = $arFields['ARTICUL']; 	
		}
		
		if($arFields['WEIGHT']){
			$this->bodyItemMS['weight'] = $arFields['WEIGHT']; 
		}
		if($arFields['VOLUME']){
			$this->bodyItemMS['volume'] = $arFields['VOLUME']; 	
		}
		
		if($arSectionMSInfo[$arFields['EXT_CODE_SECTION']]['FOLDER']['meta']){
			$this->bodyItemMS['productFolder']['meta'] = $arSectionMSInfo[$arFields['EXT_CODE_SECTION']]['FOLDER']['meta']; 	
		}
		
		$this->bodyItemMS['externalCode'] = $arFields['EXT'];
		
		/*if($arFields['content_img'] && $arFields['name_img']){
			$this->bodyItemMS['image'] = array(
				'filename' => $arFields['name_img'], 
				'content' => $arFields['content_img']
			);
		}*/

		
		$this->bodyItemMS['salePrices'][0] = array('value'=> $arFields['PRICE'], 'currency' => array(),'priceType' => "Цена продажи");

		$this->bodyItemMS['salePrices'][0]['currency']['meta'] = $arFields['currency'];

		$this->arMainCurrency = $arFields['currency'];


		$this->bodyItemMS['buyPrice']['currency']['meta'] = $arFields['alt_currency'];
		
		$this->arAltCurrency = $arFields['alt_currency'];

		$this->bodyItemMS['buyPrice']['value'] = 0;


	}

	public static function getSectionsInfoSite($idIBlock){
		self::$arSectionSiteInfo = array();
		$dbSection = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>$idIBlock/*,"ACTIVE"=>"Y"*/),false,array("ID","XML_ID","NAME","IBLOCK_SECTION_ID"));
		$arBindsXMLID = array();

		while($rs = $dbSection->Fetch()){
			if($rs['XML_ID']){
				$arBindsXMLID[$rs['ID']] = $rs['XML_ID'];
				self::$arSectionSiteInfo[$rs['ID']] = array(
					"NAME"=>$rs['NAME'],
					'EXT_CODE'=>$rs['XML_ID'],
					"PARENT"=>$rs["IBLOCK_SECTION_ID"]
				);

			}
		}
		
		foreach (self::$arSectionSiteInfo as &$arSection){
			if($arBindsXMLID[$arSection["PARENT"]]){
				$arSection['PARENT_XML_ID'] = $arBindsXMLID[$arSection["PARENT"]];	
			}
			
		}
	}

	public static function getItemsSite($idIBlock, $stepExchange, $offset, $idTypePrice){
		self::$arSiteItems = array(); 
		$dbElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$idIBlock/*, "ACTIVE"=>"Y"*/),false,
			array('nPageSize'=>$stepExchange,'iNumPage'=>$offset, 'checkOutOfRange' => true),
			array("ID","NAME","XML_ID","IBLOCK_SECTION_ID","PROPERTY_CML2_ARTICLE"));
		
		while ($re = $dbElement->Fetch()){

		    $resultPrice = CPrice::GetList(array(),array("PRODUCT_ID"=>$re['ID'], "CATALOG_GROUP_ID"=>$idTypePrice),false,false,array("PRICE"))->Fetch();
		    $priceGood = $resultPrice['PRICE'] ? $resultPrice['PRICE']*100 : 0;

			self::$arSiteItems[$re['XML_ID']] = array(
				'ID' => $re['ID'],
				'NAME' => $re['NAME'],
				'EXT' => $re['XML_ID'],
				'IBLOCK_SECTION_ID'=> $re['IBLOCK_SECTION_ID'],
				'EXT_CODE_SECTION' => self::$arSectionSiteInfo[$re['IBLOCK_SECTION_ID']]['EXT_CODE'],
				"ARTICUL" => $re['PROPERTY_CML2_ARTICLE_VALUE'],
				"PRICE" => $priceGood
			);
		}

	}
	
	public function setBuyPriceSite($idItem){
		
		$dbPrice = CPrice::GetList(
			array(), 
			array("PRODUCT_ID"=>$idItem, "CATALOG_GROUP_ID"=>6), 
			false, false, array("PRICE","CURRENCY"))->Fetch();
		
		
		if($dbPrice['CURRENCY'] == 'RUB'){
			$this->bodyItemMS['buyPrice']['currency']['meta'] = $this->arMainCurrency;
		}elseif($dbPrice['CURRENCY'] == 'CNY'){
			$this->bodyItemMS['buyPrice']['currency']['meta'] = $this->arAltCurrency;
		}
		
		if($dbPrice['PRICE']){
			return $this->bodyItemMS['buyPrice']['value'] = $dbPrice['PRICE'] * 100;
		}else{
			return $this->bodyItemMS['buyPrice']['value'] = 0;
		}
	}

	public function getCurrentPriceSite($idItem, $typePrice, $allTypesPriceMainMS, $allTypesPriceCityMS){
		
		// print_r($allTypesPriceMainMS);

		$dbPrice = CPrice::GetList(
			array(), 
			array("PRODUCT_ID"=>$idItem, "CATALOG_GROUP_NAME"=>$allTypesPriceMainMS), 
			false, false, array("CATALOG_GROUP_ID","PRICE","CATALOG_GROUP_NAME","CURRENCY"));
		
		$this->bodyItemMS['salePrices'] = array();
		
		// print_r($allTypesPriceCityMS);

		$altAllTypesPriceCityMS = array();
		foreach ($allTypesPriceCityMS as $namePrice) {
			$altAllTypesPriceCityMS[] = strtolower($namePrice);
		}

		while ($rp = $dbPrice->Fetch()){
			

			if(in_array(strtolower($rp['CATALOG_GROUP_NAME']), $altAllTypesPriceCityMS)){
			// if(in_array($rp['CATALOG_GROUP_NAME'], $allTypesPriceCityMS)){
				$positionNamePrice = array_search(strtolower($rp['CATALOG_GROUP_NAME']), $altAllTypesPriceCityMS);

				if($rp['CURRENCY'] == 'CNY'){
					$currentCurrency = array('meta' => $this->arAltCurrency);
				}else{
					$currentCurrency = array('meta' => $this->arMainCurrency);
				}

				$this->bodyItemMS['salePrices'][] = array(
					'value' => (!empty($rp['PRICE']) ? $rp['PRICE'] * 100 : 0),
					'currency' => $currentCurrency,
					// 'priceType' => $rp['CATALOG_GROUP_NAME'],
					'priceType' => $allTypesPriceCityMS[$positionNamePrice],
				);
			}
		}


		
		// if($dbPrice['PRICE']){
		// 	return $this->bodyItemMS['salePrices'][0]['value'] = $dbPrice['PRICE'] * 100;
		// }else{
		// 	return $this->bodyItemMS['salePrices'][0]['value'] = 0;
		// }

		/*$dbPrice = CPrice::GetList(
			array(), 
			array("PRODUCT_ID"=>$idItem, "CATALOG_GROUP_ID"=>$typePrice), 
			false, false, array("PRICE"))->Fetch();
		
		if($dbPrice['PRICE']){
			return $this->bodyItemMS['salePrices'][0]['value'] = $dbPrice['PRICE'] * 100;
		}else{
			return $this->bodyItemMS['salePrices'][0]['value'] = 0;
		}*/
	}

	public function Add($connect){
		
		$url = 'https://online.moysklad.ru/api/remap/1.1/entity/product';
		// print_r('beg  adddddd');
		// echo '<pre>';
		// print_r($this->bodyItemMS);
		// echo '</pre>';
		// print_r('<br />');
		// print_r('ADDD');
		// print_r('<br />');
		print_r($this->bodyItemMS);

		$resultAdd = $connect->postRequest($url, $this->bodyItemMS); 
		return $resultAdd; 
		// print_r('<br />');
		// print_r('END ADDD');

		// MSETools::setFlag('addResult', $resultAdd);

		// file_put_contents($_SERVER['DOCUMENT_ROOT'].'/test_add_result.txt', print_r($resultAdd, true), FILE_APPEND);

		// print_r('adddddd');
	}

	public function Update($connect, $idElement){

		$url = 'https://online.moysklad.ru/api/remap/1.1/entity/product/'.$idElement;
		$resultUpdate = $connect->putRequest($url, $this->bodyItemMS); 
		
		print_r($resultUpdate);

		return $resultUpdate; 

		// MSETools::setFlag('updateResult', $resultUpdate);

		// file_put_contents($_SERVER['DOCUMENT_ROOT'].'/test_update_result.txt', print_r($resultUpdate, true), FILE_APPEND);


	}


}?>