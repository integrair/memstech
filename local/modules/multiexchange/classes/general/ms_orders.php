<?
use Bitrix\Main\Web\HttpClient;
use Bitrix\Iblock\ElementTable;

class MSOrdersExchange{
	
	private $httpConnect;
	public $bodyOrder;
	public $bodyContractor; 
	public $msOrganization;
	public $bodyPositions;  

	public function __construct($login, $password){
		$this->httpConnect = new HttpClient(); 
	    $this->httpConnect->setHeader('Content-Type', 'application/json; charset=utf-8', true); 
	    $this->httpConnect->setAuthorization($login, $password);
	}

	public function getRequest($url){
		$request = $this->httpConnect->get($url); 
		$temp = json_decode($request, true);
		return $temp;
	}

	public function postRequest($url, $arBody){
      $temp = json_decode(
        $this->httpConnect->post($url, json_encode($arBody)), true
      );
      return $temp; 
	}
	
	public function putRequest($url, $arBody){
      $temp = json_decode(
        $this->httpConnect->query('PUT', $url, json_encode($arBody)), true
      );
      return $temp; 
	}
	  
	public function isExistOrder($id){

	  $orderInfo = CSaleOrder::GetByID($id);
	  if($orderInfo['XML_ID']){
	  	$id = $orderInfo['XML_ID']; 
	  }

	  $reqMS = json_decode($this->httpConnect->get('https://online.moysklad.ru/api/remap/1.1/entity/customerorder?filter=externalCode%3D'.$id), true);

	  if($reqMS['rows'][0]['name']){
	    return $reqMS['rows'][0];
	  }else{
	    return false;
	  }
	}	
	
	public function setOrganization(){
		$reqOrganization = json_decode($this->httpConnect->get('https://online.moysklad.ru/api/remap/1.1/entity/organization'), true);
		if(!empty($reqOrganization['rows'][0])){
			$arOrganization = $reqOrganization['rows'][0]['meta']; 
			unset($arOrganization['metadataHref']);
			unset($arOrganization['uuidHref']);
			$this->msOrganization = $arOrganization; 
			return true; 
		}else{
			return false; 
		}
		
	}
	
	public function isContractor($externalCode){
	  $reqContractor = json_decode($this->httpConnect->get('https://online.moysklad.ru/api/remap/1.1/entity/counterparty?filter=externalCode%3D'.urlencode($externalCode)), true);

	  if($reqContractor['rows'][0]['name']){
	    return $reqContractor['rows'][0];
	  }else{
	    return false;
	  }
	}

	public function createContractor(){
		if(!empty($this->bodyContractor)){
			$url = 'https://online.moysklad.ru/api/remap/1.1/entity/counterparty'; 
			$reqCreateContractor = $this->postRequest($url, $this->bodyContractor);
			
			print_r($reqCreateContractor) ;

			return $reqCreateContractor; 
		}
		return false;
	}
	
	public function createNewOrder(){
		if(!empty($this->bodyOrder)){
			$url = 'https://online.moysklad.ru/api/remap/1.1/entity/customerorder'; 
			$reqCreateOrder = $this->postRequest($url, $this->bodyOrder); 
			return $reqCreateOrder; 
		}
		return false;
	}
	public function testItemInMS($xmlID){
		  $reqItem = json_decode($this->httpConnect->get('https://online.moysklad.ru/api/remap/1.1/entity/product?filter=externalCode%3D'.$xmlID), true);
		  
		  if($reqItem['rows'][0]['name']){
		    return $reqItem['rows'][0];
		  }else{
		    return false;
		  }		
	}

	public function addNewPositionInOrder($idOrder){
		if(!empty($this->bodyPositions) && $idOrder){
			$url = 'https://online.moysklad.ru/api/remap/1.1/entity/customerorder/'.$idOrder.'/positions'; 

			$reqAddPos = $this->postRequest($url, $this->bodyPositions); 
			return $reqAddPos;

		}
		return false;
	}

	public function addNewCounterpartySite($arFields){
		// $arFields['LOGIN'] = 'test_user6000';
		if(!empty($arFields)){
			
			$dbUser = CUser::GetList(
				($by="personal_country"),
				($order="desc"),
				array("XML_ID"=>$arFields['XML_ID']),
				array("FIELDS"=>array("ID","NAME","EMAIL","LAST_NAME","UF_CLOSE_CITY","UF_ORGANIZATION")))->Fetch();
			if($dbUser['ID']){ 
			
				$msInfoUser = $this->isContractor($arFields['XML_ID']); 
				
				$emailMS = $msInfoUser['email'];
				$nameUserMS = $msInfoUser['name'];
				$legalTitle = $msInfoUser['legalTitle'];

				$arFieldsUpdate = array(); 
				
				if($nameUserMS != $dbUser['NAME']){
					$arFieldsUpdate['NAME'] = $nameUserMS; 
				}

				if($emailMS != $dbUser['EMAIL']){
					$arFieldsUpdate['EMAIL'] = $emailMS; 	
				}

				if($legalTitle != $dbUser["UF_ORGANIZATION"]){
					$arFieldsUpdate['UF_ORGANIZATION'] = $legalTitle; 		
				}
				
				if(!empty($arFieldsUpdate)){
					self::updateSiteContractor($dbUser['ID'], $arFieldsUpdate); 
				}


				return $dbUser['ID']; 


			}

			$newUser = new CUser; 
			$idNewUser = $newUser->Add($arFields);
			echo $newUser->LAST_ERROR;

		    /* Отправляем сообщение новому пользователю */
            
            if($idNewUser){
			    $arEventFields = array(
			        "EMAIL" => $arFields['EMAIL'],
			        "CONFIRM_CODE" => randString(8),
			        "USER_ID" => $idNewUser,
			        "LOGIN" => $arFields['LOGIN'],
			        "NAME" => $arFields['NAME'],
			        "PASSWORD" => $arFields['PASSWORD']
			    );

			    $event = new CEvent;
			    $event->SendImmediate("NEW_USER", 's1', $arEventFields);
			    $event->SendImmediate("MS_NEW_USER_CONFIRM", 's1', $arEventFields);
            }

            /*END Отправляем сообщение новому пользователю  */


			return $idNewUser;
		}else{
			return false;
		}
	}

	public static function updateSiteContractor($idUser, $arFieldsUpdate){
        $user = new CUser;

        $user->Update($idUser, $arFieldsUpdate);
        $strError .= $user->LAST_ERROR;

    }

	public static function generatePassword($maxLength = 8){

	    $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP"; 
	    $size=StrLen($chars)-1; 
	    $password=null; 

	    while($maxLength--) 
	        $password.=$chars[rand(0,$size)]; 

	    return $password; 

	}
	
	public static function addNewOrderSite($arFields, $arItems, $location){

		// if (CModule::IncludeModule("statistic"))
		//    $arFields["STAT_GID"] = CStatistic::GetEventParam();

		$orderID = CSaleOrder::Add($arFields);
		global $APPLICATION; 
		if($ex = $APPLICATION->GetException()) echo $ex->GetString(); 
		
		$arFieldsProp = array(
		   "ORDER_ID" => $orderID,
		   "ORDER_PROPS_ID" => 6,
		   "NAME" => "Местоположение",
		   "CODE" => "LOCATION",
		   "VALUE" => $location
		);

		CSaleOrderPropsValue::Add($arFieldsProp);

		foreach ($arItems as $arItem) {
			$xmlID = $arItem['XML_ID'];
			$dbItem = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>22, "XML_ID"=>$xmlID),
				false,false,array("ID","NAME","IBLOCK_SECTION_ID"))->Fetch();
			if($dbItem['ID']){
				$arFieldsItem = array(
					"PRODUCT_ID" => $dbItem['ID'],
					// "PRODUCT_PRICE_ID" => 0,
					"PRICE" => $arItem['PRICE'],
					"CURRENCY" => "RUB",
					'PRODUCT_XML_ID' => $xmlID,
					// "WEIGHT" => 530,
					"QUANTITY" => $arItem['QUANTITY'],
					"LID" => LANG,
					"DELAY" => "N",
					"CAN_BUY" => "Y",
					"ORDER_ID" => $orderID,
					"NAME" => $dbItem['NAME'],
					// "CALLBACK_FUNC" => "MyBasketCallback",
					// "MODULE" => "my_module",
					// "NOTES" => "",
					// "ORDER_CALLBACK_FUNC" => "MyBasketOrderCallback",
					"DETAIL_PAGE_URL" => "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=22&type=aspro_optimus_catalog&ID=".$dbItem['ID']."&lang=ru&find_section_section=".$dbItem['IBLOCK_SECTION_ID']."&WF=Y"
				);

				CSaleBasket::Add($arFieldsItem);
				if($ex = $APPLICATION->GetException()) echo $ex->GetString();

			}

		}
		return $orderID;
	}

	public function getUseMSInfo($url){
		$result = $this->getRequest($url);
		if($result['id']){
			$arUser = array(
				"name" => $result['name'],
				"email" => $result['email'],
				"ext_code" => $result['externalCode'],
				"legalTitle"=> $result['legalTitle']
			);
			return $arUser; 			
		}else{
			return false;
		}
		
	}

	public function getCurrentStateMS($url){
		$result = $this->getRequest($url);
		if($result['id']){
			return $result['name'];
		}else{
			return false;
		}		
	}

	public function getPositionsMS($url){
		$result = $this->getRequest($url);
		if(!empty($result['rows'])){
			$arItems = array();
			foreach ($result['rows'] as $ar_item_ms) {
				$item = array(
					"PRICE" => $ar_item_ms['price'],
					"QUANTITY" => $ar_item_ms['quantity'],
					"DISCOUNT" => $ar_item_ms['discount']
				);
				$infoItem = $this->getRequest($ar_item_ms['assortment']['meta']['href']);
				$item['NAME'] = $infoItem['name'];
				$item['XML_ID'] = $infoItem['externalCode'];
				$arItems[] = $item; 
			}
			return $arItems; 
		}else{
			return false;
		}

	}

	public static function isOrderExistOnSite($extCode){
		if($extCode){
			
			if(strlen($extCode) > 10){
				$dbOrder = CSaleOrder::GetList(array(),array("XML_ID" => $extCode),false,false,array())->Fetch();
				 // return false; 
			}else{
				$dbOrder = CSaleOrder::GetList(array(),array("ID" => $extCode),false,false,array())->Fetch();
			}

			
			if(!$dbOrder['ID']){ return false; }

			$arOrderSite = array(
				"ID" => $dbOrder['ID'],
				"USER_ID" => $dbOrder['USER_ID'],
				"STATUS_ID" => $dbOrder['STATUS_ID'],
				"PRICE" => $dbOrder['PRICE']
			);
			return $arOrderSite; 
		}else{
			return false;
		}
		
	}

    public static function compareTwoDateMicroSeconds($dataMS, $dateSite, $dotsFormat){

      if(!$dataMS || !$dateSite){ return false; }
      $timestampMS = MakeTimeStamp($dataMS, "YYYY-MM-DD HH:MI:SS"); 
      if($dotsFormat){
        $timestampSite = MakeTimeStamp($dateSite, "DD.MM.YYYY HH:MI:SS");
      }else{
        $timestampSite = MakeTimeStamp($dateSite, "YYYY-MM-DD HH:MI:SS");
      }
      
      $diffMicroSecond = $timestampMS - $timestampSite;
      return $diffMicroSecond; 

    }

    public function updateOrderExtCode($arOrderMS, $newExtCode){
    	if(!empty($arOrderMS) && $newExtCode){
    		$newExtCode = (string)$newExtCode; 
	    	$urlOrder = $arOrderMS['meta']['href']; 
	    	$arOrderBody['externalCode'] = $newExtCode; 
	    	$arOrderBody['id'] = $arOrderMS['id'];
	    	$result = $this->putRequest($urlOrder, $arOrderBody);
	    	return $result; 
    	}else{
    		return false; 
    	}
    }

      public function getMSStates(){
    
	    $arStates = array();
	    $statesListReq = $this->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/customerorder/metadata');
	    foreach($statesListReq['states'] as $stateMS){
	      $arStates[$stateMS['id']] = $stateMS['name'];
	    }
	    return $arStates; 

	  }
      
      public function getMSStatesLetters(){
    
	    $arStates = array();
	    $statesListReq = $this->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/customerorder/metadata');
	    foreach($statesListReq['states'] as $stateMS){
	      $arStates[$stateMS['id']] = preg_replace('/^(\[(.+)\])?.+$/', '\2', $stateMS['name']);
	    }
	    return $arStates; 

	  }	  




	public function createPayDoc($msOrder, $idOrder){
   
   		if(!empty($msOrder) && $idOrder){
			$dbOrder = CSaleOrder::GetByID($idOrder);   

			$bodyRequestCreatePayDoc = array();
			$bodyRequestCreatePayDoc['organization'] = $msOrder['organization'];
			$bodyRequestCreatePayDoc['agent'] = $msOrder['agent'];
			$bodyRequestCreatePayDoc['name'] = $idOrder.'';
			$bodyRequestCreatePayDoc['operations'][0]['meta'] = $msOrder['meta'];
			$bodyRequestCreatePayDoc['operations'][0]['linkedSum'] = $dbOrder['PRICE']*100;
			$bodyRequestCreatePayDoc['sum'] = $dbOrder['PRICE']*100;
			$url = 'https://online.moysklad.ru/api/remap/1.1/entity/paymentin'; 
			$this->postRequest($url, $bodyRequestCreatePayDoc); 
   		}else{
   			return false;
   		}

 	}

 	public function getDeliveryExt(){
 		$filterLine = urlencode('externalCode=deliveryorder');
		$request = $this->getRequest('https://online.moysklad.ru/api/remap/1.1/entity/product?filter='.$filterLine); 
		if(!empty($request['rows'])){
			return $request['rows'][0];
		}else{
			$arBody = array(
				"name" => "Доставка заказа",
				"externalCode" => 'deliveryorder'
			);
			$requestAdd = $this->postRequest('https://online.moysklad.ru/api/remap/1.1/entity/product', $arBody); 
			return $requestAdd; 
		}

 	}

	public function updateStateInMS($idStateMS, $arOrderMS){
		$arOrderMS['state']['meta']['href'] = 'https://online.moysklad.ru/api/remap/1.1/entity/customerorder/metadata/states/'.$idStateMS;
		$resultUpdate = $this->putRequest($arOrderMS['meta']['href'], $arOrderMS); 
		return $resultUpdate; 


	}

	public static function checkPayedProp($idOrder, $codePayed){
	    $dbPropOrder = CSaleOrderPropsValue::GetList(array(),array("CODE"=>"STATUS_PAYED","ORDER_ID"=>$idOrder),false,false,array("ID","VALUE","NAME"))->Fetch();
	    
	    if($codePayed == $dbPropOrder['VALUE']){ return false; }

	    if($dbPropOrder['ID']){
	        CSaleOrderPropsValue::Update($dbPropOrder['ID'], array("VALUE" => $codePayed));
	    }else{
	        $arFields = array(
	           "ORDER_ID" => $idOrder,
	           "ORDER_PROPS_ID" => 20,
	           "NAME" => "Оплачено",
	           "CODE" => "STATUS_PAYED",
	           "VALUE" => $codePayed
	        );

	        CSaleOrderPropsValue::Add($arFields);
	    }
	}

	public function addIDOrderSiteToMS($idNewOrder, $arOrderMS){
		$url = 'https://online.moysklad.ru/api/remap/1.1/entity/customerorder/metadata';
		$resultRequest = $this->getRequest($url); 
		if(!empty($resultRequest['attributes'])){
			$arMeta = array();
			foreach ($resultRequest['attributes'] as $arField) {
				if($arField['name'] == 'Заказ на сайте'){
					$idOrderNameField = $arField['id']; 
				}
			}
			if(!$idOrderNameField){ return false; }

			if($arOrderMS['meta']['href']){
				$urlUpdateOrder = $arOrderMS['meta']['href'];
				
				$arOrderAttrIDSite['attributes'][] = array(
						'id' => $idOrderNameField,
					    'value' => (string)$idNewOrder
				);

				$this->putRequest($urlUpdateOrder, $arOrderAttrIDSite);
			}
			
		}

	}






}
?>