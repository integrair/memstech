<? 

class MSEXchangeLog{

	const SQLite_LOG_PATH = '/bitrix/ms_flags/LOG.db';
	public $counter; 

	public function __construct(){
		$this->counter = new SQLite3($_SERVER['DOCUMENT_ROOT'].self::SQLite_LOG_PATH);
        $queryLineNewTable = "CREATE TABLE LOG(
            ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            DATA INTEGER NOT NULL,
            MESSAGE TEXT,
            TYPE INTEGER,
            EXTRA TEXT
        )";
        $this->counter->querySingle($queryLineNewTable);
        // print_r($this->counter->lastErrorMsg());
	}

	public function addNewLogLine($arLog){
		if(is_string($arLog)){
			$currentLine = $arLog; 
			$arLog = array();
			$arLog['message'] = $currentLine; 
		}
		$data = (string)date('h:i:s d/m/Y ', time());
		if(!$arLog['type']){ $arLog['type'] = '1';}
		if(!$arLog['extra']){ $arLog['extra'] = '';}

		$lineQuery = "INSERT INTO LOG (DATA, MESSAGE, TYPE, EXTRA) VALUES ('".$data."', '".$arLog["message"]."', ".$arLog["type"].", '".$arLog["extra"]."')";
		$this->counter->querySingle($lineQuery);
		// print_r($this->counter->lastErrorMsg());
    }


    public function printLogTest(){
        $lineQuery ="SELECT * FROM LOG";

        $result = $this->counter->query($lineQuery);
        $arResult = array();
        while ($row = $result->fetchArray()) {
		    $arResult[] = array(
				'DATA' => $row['DATA'],
			    'MESSAGE' => $row['MESSAGE'],
			    'TYPE' => $row['TYPE'],
			    'EXTRA' => $row['EXTRA'],
		    );

		}

		return $arResult; 
    }

	
}

?>