<?

use Bitrix\Main\Web\HttpClient;
use Bitrix\Iblock\ElementTable;

class MSETools{

	const PATH_TO_CONFIG = '/bitrix/ms_flags/config.db';
	public static $sqliteConfig; 
	
	public static function initSQLConfigDB(){
		self::$sqliteConfig = new SQLite3($_SERVER['DOCUMENT_ROOT'].self::PATH_TO_CONFIG);
	}

	/*public static function initLastUpdateOrdersMSTable(){
		self::initSQLConfigDB();
		if(self::$sqliteConfig){
			$queryLineNewTable = "CREATE TABLE LAST_UPDATE_ORDERS_MS(
	            ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	            ORDERID INTEGER NOT NULL,
	            LOGIN TEXT NOT NULL
	        )";
	        self::$sqliteConfig->querySingle($queryLineNewTable);
	        print_r(self::$sqliteConfig->lastErrorMsg());
		}
	}

	public static function addOrderLastUpdateMS($idOrder, $login){
		if(!$idOrder || !$login){ return false; }

		$lineQuery = "INSERT INTO LAST_UPDATE_ORDERS_MS (ORDERID, LOGIN) VALUES (".(int)$idOrder.", '".$login."')";

		self::$sqliteConfig->querySingle($lineQuery);
		print_r(self::$sqliteConfig->lastErrorMsg());
	}

	public static function deleteOrderLastUpdateMS($idOrder, $login){
		
	}*/

	public static function dropTableConfig($nameTable){
		if(!$nameTable){ return false; }

		$lineQuery = "DROP TABLE ".$nameTable;

		self::$sqliteConfig->querySingle($lineQuery);
		print_r(self::$sqliteConfig->lastErrorMsg());
	}

	public static function initExceptionPriceTypeForSynch(){
		self::initSQLConfigDB(); 
		if(self::$sqliteConfig){
			$queryLineNewTable = "CREATE TABLE PTYPES(
	            ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	            PRICETYPE INTEGER NOT NULL
	        )";
	        self::$sqliteConfig->querySingle($queryLineNewTable);
	        // print_r(self::$sqliteConfig->lastErrorMsg());
		}

        
	}

	/*public static function printConfigAlt(){
		self::initSQLConfigDB(); 
		if(self::$sqliteConfig){
			$lineQuery ="SELECT * FROM LAST_UPDATE_ORDERS_MS";
			$result = self::$sqliteConfig->query($lineQuery);
	        $arResult = array();
	        while ($row = $result->fetchArray()) {
	        	print_r($row);
			}
			return $arResult; 
		}
        

        
    }*/

	public static function printConfig(){
		self::initSQLConfigDB(); 
		if(self::$sqliteConfig){
			$lineQuery ="SELECT * FROM PTYPES";
			$result = self::$sqliteConfig->query($lineQuery);
	        $arResult = array();
	        while ($row = $result->fetchArray()) {
	        	print_r($row);
			}
			return $arResult; 
		}
        

        
    }

	public static function addPricesTypeToException($arPriceInfo){
		if(!empty($arPriceInfo)){
			self::initExceptionPriceTypeForSynch(); 
			foreach ($arPriceInfo as $idTypePrice) {
				
				if(!self::testPriceTypeInException($idTypePrice)){
					$lineQuery = "INSERT INTO PTYPES (PRICETYPE) VALUES ('".$idTypePrice."')";
					self::$sqliteConfig->querySingle($lineQuery);
				}
				
			}
		}
	}

	public static function testPriceTypeInException($idPrice){
		// self::initSQLConfigDB(); 

		if(!self::$sqliteConfig){ self::initSQLConfigDB();  }

		$lineTestQuery = "SELECT name FROM sqlite_master WHERE type='table' AND name='PTYPES'";
		$result = self::$sqliteConfig->query($lineTestQuery)->fetchArray();
		
		if($result['name'] != 'PTYPES'){ return false; }
		
		$lineQuery ="SELECT PRICETYPE FROM PTYPES WHERE PRICETYPE=".$idPrice;

        $result = self::$sqliteConfig->query($lineQuery)->fetchArray();
        if($result['PRICETYPE']){
        	return true;
        }
        return false;			 
	}

	public static function collectTypePrice(){

		$dbCity = CIBlockElement::GetList(
					array(),
					array("IBLOCK_ID"=>18, 'PROPERTY_CHECK_SYNCH'=>30),
					false,
					false,
					array("PROPERTY_LOGIN","PROPERTY_PASSWORD")
		)->Fetch();

		$loginMainSynch = $dbCity['PROPERTY_LOGIN_VALUE'];
		$passwordMainSynch = $dbCity['PROPERTY_PASSWORD_VALUE'];

		if(!$loginMainSynch || !$passwordMainSynch){ return false; }

		$httpConnect = new HttpClient(); 
	    $httpConnect->setHeader('Content-Type', 'application/json; charset=utf-8', true); 
	    $httpConnect->setAuthorization($loginMainSynch, $passwordMainSynch);
	    $urlTestMeta = 'https://online.moysklad.ru/api/remap/1.1/entity/product/metadata';
	    $request = $httpConnect->get($urlTestMeta); 
		$temp = json_decode($request, true);

		if(!empty($temp['priceTypes'])){
			$pricesMainAccount = array("Закупочная","Минимальная"); 
			foreach ($temp['priceTypes'] as $arPriceInfo) {
				$pricesMainAccount[] = $arPriceInfo['name'];
			}

			$idPricesException = array(); 
			$dbPrices = CCatalogGroup::GetList(array(), array("NAME" => $pricesMainAccount), false,false,array("ID","NAME"));
			while ($rp = $dbPrices->Fetch()) {
				$idPricesException[] = $rp['ID'];
			}

			if(!empty($idPricesException)){
				self::addPricesTypeToException($idPricesException); 
			}

		}

	}

	public static function setFlag($key, $val){
		if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags')){ mkdir($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags'); }
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/EXCHANGE')){
			$currentAr = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/EXCHANGE'));
			$arWrite = $currentAr; 
		}else{
			$arWrite = array();
		}

		if(($val === 0) || $val){
			$arWrite[$key] = $val;
		}else{
			unset($arWrite[$key]); 
		}
			
		
		
		$lineWrite = serialize($arWrite); 
		file_put_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/EXCHANGE', $lineWrite); 
	}

	public static function getFlag($key){
		if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags')){ return false; }
		
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/EXCHANGE')){
			$currentAr = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/EXCHANGE'));
			if($currentAr[$key]){
				return $currentAr[$key];
			}else{
				return false;
			}
		}
	}

	public static function addOrderToProcessing($idOrder){
		
		if(!$idOrder){ return false; }

		if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags')){ mkdir($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags'); }
		
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/ORDERS')){
			$currentAr = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/ORDERS'));
			$arWrite = $currentAr; 
		}else{
			$arWrite = array();
		}

		if(!in_array($idOrder, $arWrite['PROC_ORDER'])){
			$arWrite['PROC_ORDER'][] = $idOrder;	
		}
		

		$lineWrite = serialize($arWrite); 
		file_put_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/ORDERS', $lineWrite); 
	}

	public static function delOrderFromProcessing($idOrder){
		if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags')){ return false; }
		
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/ORDERS')){
			$currentAr = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/ORDERS'));

			if(in_array($idOrder, $currentAr['PROC_ORDER'])){

				$keyDelOrder = array_search($idOrder, $currentAr['PROC_ORDER']);
				
				print_r($keyDelOrder); 

				unset($currentAr['PROC_ORDER'][$keyDelOrder]);
				$lineWrite = serialize($currentAr); 
				file_put_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/ORDERS', $lineWrite); 
			}else{
				return false;
			}
		}
	}

	public static function getActualOrdersProcessing(){
	
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/ORDERS')){
			$currentAr = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/ms_flags/ORDERS'));

			if(!empty($currentAr['PROC_ORDER'])) {
				return $currentAr['PROC_ORDER']; 
			}else{
				return false;
			}
		}
	}
	
	public static function addItemPrice($id, $xmlPrice, $price, $currency){

	    if($xmlPrice){
	        $dbMyTypePrice = CCatalogGroup::GetList(array(),array("XML_ID"=>$xmlPrice), false, false)->Fetch();
	        if($dbMyTypePrice['ID']){
	            $PRICE_TYPE_ID = $dbMyTypePrice['ID']; 
	        }
	        
	    }

	    $arFields = Array(
	        "PRODUCT_ID" => $id,
	        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
	        "PRICE" => $price,
	        "CURRENCY" => $currency,
	        "QUANTITY_FROM" =>false,
	        "QUANTITY_TO" => false
	    );

	    $res = CPrice::GetList(
	            array(),
	            array(
	                    "PRODUCT_ID" => $id,
	                    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
	                )
	        );

	    if ($arr = $res->Fetch())
	    {
	        $result = CPrice::Update($arr["ID"], $arFields);
	    }
	    else
	    {
	        $result = CPrice::Add($arFields);
	    }
	    return $result;
	}

}

?>