<?php
CModule::IncludeModule("multiexchange");

$arClasses=array(
    'MECitys'=>'classes/general/citys.php',
    'MSExchange'=>'classes/general/exchange.php',
    'MSETools'=>'classes/general/tools.php',
    'MSEItems'=>'classes/general/ms_items.php',
    'MSOrdersExchange'=>'classes/general/ms_orders.php',
    'MSCounterItemCity' => 'classes/general/counter.php',
    'MSEXchangeLog' => 'classes/general/mslogs.php'

);

CModule::AddAutoloadClasses("multiexchange", $arClasses);
