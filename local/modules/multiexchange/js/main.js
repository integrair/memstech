$(document).ready(function(){
	function printSuccess(message){
		$("div.information").text(message);
		$("div.information").addClass("success"); 
		$("div.information").show();
	}
	
	function printError(message){
		$("div.information").text(message);
		$("div.information").addClass("error"); 
		$("div.information").show();

	}

	$("input#create-button").click(function(){
		$("div.information").hide();
		var nameNewCity = $("input#name_new_city").val();
		var code = $("input#code_new_city").val();
		var login = $("input#login_new_city").val();
		var password = $("input#pass_new_city").val();
		$.get('/local/modules/multiexchange/ajax/main.php',
			{'action':'create_city','name':nameNewCity,'code':code,'login':login, 'password':password},
			function(data){

			var result = JSON.parse(data); 

			if(result.ERROR_PRINT && (result.ERROR_PRINT.length > 0)){
				console.log(result); 	
				var resultMessageError = ''; 
				for(var line in result.ERROR_PRINT){
					resultMessageError += result.ERROR_PRINT[line] + "\n"; 
				}
				printError(resultMessageError); 
			}else if(result.SUCCESS == 'Y'){
				printSuccess('Новый город добавлен');
				BX.reload(); 
			}
			
		})
		return false;
	})


	$("a.test-connect-city").click(function(){
		var idCity = $(this).parents('.item-city-list').find("input.id_city").val();
		var infoBox = $(this).parents('.item-city-list').find("span.test_connect_info");
		var myButton = $(this);
		if(myButton.hasClass('active')){ return false; }
		myButton.addClass('active');

		var allInfoBox = $("span.test_connect_info").hide();
		BX.showWait();
		allInfoBox.hide(); 
		$.get('/local/modules/multiexchange/ajax/main.php',
			{'action':'test_connect','id':idCity},
			function(data){
				var result = JSON.parse(data); 
				if(result.result.assortment){
					console.log('true');
					infoBox.text("Соединение установленно.");
					infoBox.removeClass('red'); 
					infoBox.addClass('green'); 
					infoBox.show(); 

				}else if(result.result.errors){
					console.log('false');
					infoBox.text("Ошибка соединения.");
					infoBox.removeClass('green'); 
					infoBox.addClass('red'); 
					infoBox.show(); 
				}
				myButton.removeClass('active');
			BX.closeWait();
		});
		return false;
	})

	$("a.unload-ms").click(function(){
		
	})

	$("a.change-city").click(function(){
		$(this).parents('.item-city-list').addClass("change"); 
		$(this).parents('.item-city-list').find("p.temp-block").show();
		$(this).parents('.item-city-list').find("p.main-block").hide();
		$(this).parents('.item-city-list').find("span.login-place").hide();
		$(this).parents('.item-city-list').find("span.input-login").show();
		$(this).parents('.item-city-list').find("span.password-place").hide();
		$(this).parents('.item-city-list').find("span.input-password").show();
		return false;
	})
	
	$("a.delete-city").click(function(){
		var idCity = $(this).parents('.item-city-list').find("input.id_city").val();
		if(confirm("Вы уверенны что хотите удалить этот город? Эту операцию нельзя будет отменить.")){
			$.get('/local/modules/multiexchange/ajax/main.php',
				{'action':'delete-city','id':idCity},
				function(data){
					var result = JSON.parse(data); 
					BX.reload(); 
			});
		}
		
		return false;

	})
	$("a.save-city-change").click(function(){
		var idCity = $(this).parents('.item-city-list').find("input.id_city").val();
		
		var newLogin = $(this).parents('.item-city-list').find("input.login-field").val();
		var newPassword = $(this).parents('.item-city-list').find("input.password-field").val();
		var checkSynch = $(this).parents('.item-city-list').find("input.check_synchronization").prop("checked");
		
		if(confirm("Вы уверенны что хотите изменить доступы для этого города?")){
			$.get('/local/modules/multiexchange/ajax/main.php',
				{'action':'change_city','id':idCity,'login':newLogin, 'password':newPassword, 'check_synch':checkSynch},
				function(data){
					var result = JSON.parse(data); 
					console.log(result); 
					BX.reload(); 
			});
		}

		return false;
	})


	/* Вывод информации о технических файлах обмена */
	function updateListFlags(){
		$.get('/local/modules/multiexchange/ajax/main.php',
			{'action':'get-list-flags'},
			function(data){
				// console.log(data); 
				var result = JSON.parse(data);

				if(result.html_result){
					$("div.control-flags-list").html(result.html_result); 
				}
				// console.log(data.orders);


		});
	}

	setInterval(updateListFlags, 5000); 

	$("a.bind-stores").click(function(){
		
		var idCity = $(this).parents('div.item-city-list').find("input.id_city").val();
		
		console.log(idCity);

		$("div#modal-for-binds").arcticmodal({
			beforeOpen: function(data, el){

		        $.get('/local/modules/multiexchange/ajax/main.php',{'action':'bind_stores','id_city':idCity},function(data){
		        	var result = JSON.parse(data); 
		        	console.log(result); 

		        	$("div#modal-for-binds div.ajax-container-binds").html(result.html_result);
		        })
		    },
		}); 
		return false;
	})

	$("a.bind-prices").click(function(){
		
		var idCity = $(this).parents('div.item-city-list').find("input.id_city").val();

		$("div#modal-for-binds").arcticmodal({
			beforeOpen: function(data, el){

		        $.get('/local/modules/multiexchange/ajax/main.php',{'action':'bind_prices','id_city':idCity},function(data){
		        	var result = JSON.parse(data); 

		        	console.log(result);

		        	$("div#modal-for-binds div.ajax-container-binds").html(result.html_result);

		        })
		    },
		}); 
		return false;
	})

	$("body").on("click",".add-new-prices-bind", function(){
		
		var idCity = $(this).attr("city");
		var containerPricesBinds = $(this).parents(".ajax-container-binds").find(".wrap-binds-lines");

        $.get('/local/modules/multiexchange/ajax/main.php',{'action':'bind_prices','id_city':idCity, "type":"add_select"},function(data){
        	
        	var result = JSON.parse(data); 

        	containerPricesBinds.append(result.html_result);

        })

		return false;
	})
	$("body").on("click","a#save-prices-binds",function(){
		if(confirm("Вы уверенны что хотите пересохранить привязки цен для этого города?")){
			var arBindsPrices = [];
			$("div.wrap-binds-lines div.bind-line").each(function(){
				arBindsPrices.push({
					'site':$(this).find("select.select-site-prices").val(), 
					'ms':$(this).find("select.select-ms-prices").val()
				});
			})
			
			console.log(arBindsPrices);

			var idCity = $(this).attr("city");
			$.get('/local/modules/multiexchange/ajax/main.php',{'action':'save_binds_prices','id_city':idCity, 'prices': arBindsPrices},function(data){
	        	var result = JSON.parse(data); 
	        	
	        	console.log(result);

	        	if(result.confirm == 'Y'){
	        		$("p.messsage-line-prices").text("Привязки цен обновлены успешно"); 
	        		setTimeout(function(){
	        			$("p.messsage-line-prices").text(""); 
	        		}, 2000);
	        	}

	        })
		}
		return false;
	})

	
	$("body").on("click","a#save-stores-binds",function(){
		if(confirm("Вы уверенны что хотите пересохранить привязку склада для этого города?")){
			/*var arBindsPrices = [];
			$("div.wrap-binds-lines div.bind-line").each(function(){
				arBindsPrices.push({
					'site':$(this).find("select.select-site-prices").val(), 
					'ms':$(this).find("select.select-ms-prices").val()
				});
			})*/

			var siteStoreID = $("div.wrap-binds-lines-stores div.bind-line-store select.select-site-stores").val();
			var msStoreID = $("div.wrap-binds-lines-stores div.bind-line-store select.select-ms-stores").val();
			var arBindStore = {'site':siteStoreID, 'ms':msStoreID}; 
			
			console.log(arBindStore);

			var idCity = $(this).attr("city");
			$.get('/local/modules/multiexchange/ajax/main.php',{'action':'save_binds_stores','id_city':idCity, 'stores': arBindStore},function(data){
	        	var result = JSON.parse(data); 
	        	
	        	console.log(result);

	        	if(result.confirm == 'Y'){
	        		$("p.messsage-line-stores").text("Привязка разделов успешно обновлена"); 
	        		setTimeout(function(){
	        			$("p.messsage-line-stores").text(""); 
	        		}, 2000);
	        	}

	        })
		}
		return false;
	})


	/* END */
	

})