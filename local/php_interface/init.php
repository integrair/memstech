<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
   use Bitrix\Main\EventManager; 

   $sUserCity = $APPLICATION->get_cookie('USER_CITY');
   // print_r($sUserCity); 

   $GLOBALS['sCurCity'] = preg_replace('/^(?:([^\.]+)\.)?memstech\.ru$/', '\1', $_SERVER['SERVER_NAME']);
   if(!$GLOBALS['sCurCity']){
      $GLOBALS['sCurCity'] = 'moscow'; 
   }

   $arCityCodes = $arCities = array();
   if(\Bitrix\Main\Loader::includeModule('iblock')){
      $arFilter = array('IBLOCK_ID'=>18, 'GLOBAL_ACTIVE'=>'Y', '!code'=>false);
      
      $arSelect = array("NAME","ID","CODE","PROPERTY_MAIN_CITY");

      $dbCities = CIBlockElement::GetList(array('SORT'=>'ASC'), $arFilter,false,false, $arSelect);
      
      while ($arCity = $dbCities->GetNext(false,false)) {
         $arCityCodes[] = $arCity['CODE'];
         $arCities[] = $arCity;
      }
   }   

   foreach ($arCities as $arCity) {
      if ($arCity['CODE'] == $sCurCity){
         $GLOBALS['sCurCityName'] = $arCity['NAME'];
         $GLOBALS['iCurCityID'] = $arCity['ID'];
      }
   }

   if (!in_array($sCurCity,$arCityCodes)) {
      if (in_array($sUserCity,$arCityCodes)) {
      } else {
         // $APPLICATION->set_cookie('USER_CITY', false);
         if($sCurCity){
            header("Location: http://memstech.ru/");   
         }
         
      }
   }
   
   // if (strlen($sCurCity)) $APPLICATION->set_cookie('USER_CITY', $sCurCity); 


   AddEventHandler('iblock', 'OnIBlockPropertyBuildList', 
   Array('StoresIBlockProperty','GetUserTypeDescription'), 
   100, $_SERVER["DOCUMENT_ROOT"].'/local/php_interface/tools/stores.php'
   );

   AddEventHandler('iblock', 'OnIBlockPropertyBuildList', 
   Array('CIBlockPropertyTypePrices','GetUserTypeDescription'), 
   100, $_SERVER["DOCUMENT_ROOT"].'/local/php_interface/tools/prices.php'
   );

   AddEventHandler('iblock', 'OnIBlockPropertyBuildList', 
   Array('LocationForExchange','GetUserTypeDescription'), 
   100, $_SERVER["DOCUMENT_ROOT"].'/local/php_interface/tools/location.php'
   );

AddEventHandler('main', 'OnBuildGlobalMenu', 'newLinkToCityPage');

function newLinkToCityPage(&$aGlobalMenu, &$aModuleMenu)
{

      $aModuleMenu[] = array(
         'parent_menu' => 'global_menu_services',
           'sort' => 60,
           'text' => 'Страница настройки городов',
           'title' => 'Страница настройки городов',
           'icon' => 'vote_menu_icon',
           'page_icon' => 'vote_menu_icon',    
           'url'=> 'manage_city.php?lang=ru'
      );
      $aModuleMenu[] = array(
         'parent_menu' => 'global_menu_services',
           'sort' => 65,
           'text' => 'Журнал обмена',
           'title' => 'Журнал обмена',
           'icon' => 'vote_menu_icon',
           'page_icon' => 'vote_menu_icon',    
           'url'=> 'exchange_log.php?lang=ru'
      );

}

function getLocCodeToMS($idLoc) {
    $dbCitys = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 18, "PROPERTY_LOCATION" => $idLoc), false, false, array("PROPERTY_LOCATION", "ID", "NAME"));
    $arReturn = array();
    while ($resCity = $dbCitys->Fetch()) {
        if ($resCity['ID']) {
            $arLocation = CSaleLocation::GetByID($resCity['PROPERTY_LOCATION_VALUE']);
            $arReturn[] = $arLocation['CODE'];
        }
    }
    if ($arReturn && !empty($arReturn)) {
        return $arReturn;
    }
    else{
        return false;
    }
}

CModule::IncludeModule('multiexchange');

AddEventHandler("catalog", "OnBeforeCatalogImport1C", 'preWorkSynch', 500);

AddEventHandler("catalog", "OnSuccessCatalogImport1C", 'checkExchangeAllMS', 500);

function checkExchangeAllMS(){
  
  MSETools::setFlag('city', false);
  MSETools::setFlag('currentCity', false);
  MSETools::setFlag('arCity',false);
  MSETools::setFlag('main_access',false);
  MSETools::setFlag('step','sections');
  MSETools::setFlag('offset', false);
  MSETools::setFlag('next_link_stocks', false);
  MSETools::setFlag('block',"N");
  $objCounter = new MSCounterItemCity();
  $objCounter->destroyCounter();
  
  $objLog = new MSEXchangeLog();
  $objLog->addNewLogLine('Начало обмена на JSON API');

}

function preWorkSynch(){
   unlink($_SERVER['DOCUMENT_ROOT'].MSETools::PATH_TO_CONFIG);
   MSETools::collectTypePrice();
}

$eventManager = EventManager::getInstance();

$eventManager->addEventHandler("main", "OnBeforeProlog", "checkUserLink");

function checkUserLink(){
   global $USER;
   global $APPLICATION; 

   $accountUserCity = $USER->GetParam("CODE_CITY"); 
   $fullAddress = $APPLICATION->GetCurUri(); 

   if($accountUserCity && ($GLOBALS['sCurCity'] != $accountUserCity)){
      $_SESSION['USER_CITY_CODE'] = $accountUserCity;
      header("Location: http://$accountUserCity.memstech.ru".$fullAddress); 
      // header("Location: http://$accountUserCity.memstech.ru"); 

   }elseif(!$accountUserCity){

     $dbUser = CUser::GetByID($USER->GetID())->Fetch(); 
     if($dbUser['UF_CLOSE_CITY']){
        $dbCity = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>18, "ID"=>$dbUser['UF_CLOSE_CITY']),
              false,false,array("CODE","ID","NAME","PROPERTY_PHONES","PROPERTY_LOCATION"))->Fetch();
        if($dbCity['CODE']){
           $USER->SetParam("CODE_CITY", $dbCity['CODE']);
           $USER->SetParam("NAME_CITY", $dbCity['NAME']);
           $USER->SetParam("LOCATION_CITY", $dbCity['PROPERTY_LOCATION_VALUE']);
        }
     }else{

        $_SESSION['USER_CITY_CODE'] = $GLOBALS['sCurCity'];

        /*$_SESSION['CITY_INFO']['PHONES'] = array();
        $dbCity = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>18, "CODE" => $_SESSION['USER_CITY_CODE']),false,false,array("NAME","ID","CODE","PROPERTY_PHONES")); 
        while ($dbInfoCity = $dbCity->Fetch()) {
          $_SESSION['USER_CITY_CODE'] = $dbInfoCity['CODE']; 
          $_SESSION['CITY_INFO']['LANG_NAME'] = $dbInfoCity['NAME']; 
          $_SESSION['CITY_INFO']['PHONES'][] = $dbInfoCity['PROPERTY_PHONES_VALUE']; 
        }*/


     }     
   }
}

$eventManager->addEventHandler('sale','OnOrderUpdate','onUpdateOrderCheckMS');
$eventManager->addEventHandler('sale','OnOrderSave','onSaveOrderCheckMS');
// $eventManager->addEventHandler('sale','OnSaleOrderBeforeSaved','setPropsOrderLocation');

function onUpdateOrderCheckMS($ID, $arFields){
  MSETools::addOrderToProcessing($ID);
}
function onSaveOrderCheckMS($orderId, $fields, $orderFields, $isNew){
  MSETools::addOrderToProcessing($orderId);
}

// function setPropsOrderLocation(Main\Event $event, $VALUES){
  /*if(!$arFields['ORDER_PROP'][6] && $GLOBALS['iCurCityID']){

    $dbCities = CIBlockElement::GetList(
      array('SORT'=>'ASC'),
      array('IBLOCK_ID'=>18,"ID"=> $GLOBALS['iCurCityID'], 'GLOBAL_ACTIVE'=>'Y', '!code'=>false),
      false,false,
      array("NAME","ID","CODE","PROPERTY_LOCATION"))->Fetch();

    if($dbCities['PROPERTY_LOCATION_VALUE']){
      
      $arLocation = CSaleLocation::GetByID($dbCities['PROPERTY_LOCATION_VALUE']);
      if($arLocation['CODE']){
        $arFields['ORDER_PROP'][6] = $arLocation['CODE']; 
        $arFields['DELIVERY_LOCATION'] = $arLocation['CODE'];
      }

    }
  }*/
  // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/testlocation.txt', print_r($event, true), FILE_APPEND);
  // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/testlocation.txt', print_r($VALUES, true), FILE_APPEND);
   
// }

$eventManager->addEventHandler('main','OnBeforeUserAdd','addNewFieldsToNewUser');

function addNewFieldsToNewUser(&$arParams){
  if(!$arParams['XML_ID']){
    $arParams['XML_ID'] = $arParams['EMAIL'];
  }
  
  if(!$arParams['UF_CLOSE_CITY']){
      $codeCity = $_SESSION['USER_CITY_CODE']; 
      $dbCity = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>18, "CODE"=>$codeCity),false,false,array("CODE","ID","NAME"))->Fetch();
      $idCity = $dbCity['ID'];
      $arParams['UF_CLOSE_CITY'] = $idCity;   
  }
  
}

$eventManager->addEventHandler('sale','OnOrderNewSendEmail','checkMailFieldsTest');

function checkMailFieldsTest($idOrder, $event, $arFields){
  
  if(($event == 'SALE_NEW_ORDER') && !$arFields['ORDER_LIST']){
    return false;
  }
}

$eventManager->addEventHandler('iblock','OnBeforeIBlockElementUpdate','controlActiveElement');
$eventManager->addEventHandler('iblock', "OnBeforeIBlockSectionUpdate",'controlActiveSection');

  function controlActiveSection(&$arParams){
    


    if($_SERVER['SCRIPT_NAME'] == '/bitrix/admin/1c_exchange.php'){
       $idSection = $arParams['ID'];
       $newActiveSection = $arParams['ACTIVE'];
       $dbSection = CIBlockSection::GetByID($idSection)->Fetch();

       if($dbSection['ACTIVE'] != $newActiveSection){
            $arParams['ACTIVE'] = $dbSection['ACTIVE'];
       }
     }
  }


  function controlActiveElement(&$arParams){

      /////////////////////////// Проверка активности //////////////////////////////////////////////
      
      if($_SERVER['SCRIPT_NAME'] == '/bitrix/admin/1c_exchange.php'){

        $idElement = $arParams['ID'];
        $newActiveElement = $arParams['ACTIVE'];
        $dbElement = CIBlockElement::GetByID($idElement)->Fetch();

        if($dbElement['ACTIVE'] != $newActiveElement){
          $arParams['ACTIVE'] = $dbElement['ACTIVE'];
        }

        unset($arParams['DETAIL_PICTURE']);
      }

  }


  /* Типы цен при добавлении в корзину */
  $eventManager->addEventHandler('catalog', "OnGetOptimalPrice",'priceToBasket');

  function priceToBasket($intProductID, $quantity, $arUserGroups, $renewal, $arPrices, $siteID, $arDiscountCoupons){
    CModule::IncludeModule('multiexchange');

    foreach ($arUserGroups as $idGroup) {
      if($idGroup > 8){
        $arGroupUser[] = $idGroup;
      } 
    }
  
    if(!empty($arGroupUser)){

      $dbCustomGroup = CCatalogGroup::GetGroupsList(array("GROUP_ID"=>$arGroupUser[0], "BUY"=>"Y"))->Fetch();
      

      if($dbCustomGroup['CATALOG_GROUP_ID']){
          $dbPriceCustomGroup = CPrice::GetList(
            array(),
            array("PRODUCT_ID" => $intProductID,"CATALOG_GROUP_ID" => $dbCustomGroup['CATALOG_GROUP_ID']),
            false,
            false,
            array("PRICE"))->Fetch();

          if($dbPriceCustomGroup['PRICE'] > 0){
              return array(
                "PRICE" => array(
                  'PRICE' => $dbPriceCustomGroup['PRICE'],
                  'CURRENCY' => 'RUB'
                ),
                "DISCOUNT_LIST" => array()
              );
          }
      }
    }

      $idCity = $GLOBALS['iCurCityID']; 
      $arFilter = array('IBLOCK_ID'=>18, "ID"=>$idCity, 'GLOBAL_ACTIVE'=>'Y', '!code'=>false);      
      $arSelect = array("NAME","ID","CODE","PROPERTY_PRICE");
      $dbCities = CIBlockElement::GetList(array('SORT'=>'ASC'), $arFilter,false,false, $arSelect)->Fetch();
      $idCityPrice = $dbCities['PROPERTY_PRICE_VALUE']; 
      $dbPrice = CCatalogGroup::GetByID($idCityPrice);

      $dbPriceItem = CPrice::GetList(
        array(),
        array("PRODUCT_ID" => $intProductID,"CATALOG_GROUP_ID" => $idCityPrice),false,
        false,array("PRICE"))->Fetch();

      return array(
            "PRICE" => array(
              'PRICE' => $dbPriceItem['PRICE'],
              'CURRENCY' => 'RUB'
            ),
            "DISCOUNT_LIST" => array(
                /*array(
                    'VALUE_TYPE'=> $discount_final_type,
                    'VALUE'=> $discount_final_val,
                    'CURRENCY'=>"RUB"  
                )*/
            )
      );
      

  }

  $eventManager->addEventHandler('sale', "OnSaleComponentOrderProperties",'changePropsOrder');
  
  function changePropsOrder(&$arUserResult, $request, &$arParams, &$arResult){
  

    global $USER; 
    $currentLocationID = $USER->GetParam("LOCATION_CITY");
    if($currentLocationID){
       $dbLocation = CSaleLocation::GetByID($currentLocationID);
       $symbolCityCode = $dbLocation['CODE']; 
       if($symbolCityCode){
          $arUserResult['ORDER_PROP'][6] = $symbolCityCode;
          $arUserResult['ORDER_PROP'][18] = $symbolCityCode;
       }
    }

    $dbUser = CUser::GetByID($USER->GetID())->Fetch();
    // echo '<pre>';
    // // print_r($dbUser['PERSONAL_PHONE']);
    // print_r($arUserResult['ORDER_PROP'][8]);
    // echo '</pre>';
    if(!$arUserResult['ORDER_PROP'][3]){
      $arUserResult['ORDER_PROP'][3] = $dbUser['PERSONAL_PHONE'];   
    }

    if(!$arUserResult['ORDER_PROP'][8]){
      $arUserResult['ORDER_PROP'][8] = $dbUser['UF_ORGANIZATION'];   
    }

    
  }

  $eventManager->addEventHandler('catalog', "OnBeforePriceAdd", 'controlPriceAddSynch');
  $eventManager->addEventHandler('catalog', "OnBeforePriceUpdate", 'controlPriceUpdateSynch');
  $eventManager->addEventHandler('catalog', "OnBeforePriceDelete", 'controlPriceDeleteSynch');
  $eventManager->addEventHandler('catalog', "OnGroupAdd", 'recalculatePriceException');

  function recalculatePriceException($ID, $arFields){

    if($_SERVER['SCRIPT_NAME'] == '/bitrix/admin/1c_exchange.php'){
       unlink($_SERVER['DOCUMENT_ROOT'].MSETools::PATH_TO_CONFIG);
       MSETools::collectTypePrice();
    }
    
  }
  
  function controlPriceAddSynch(&$arFields){
    if($_SERVER['SCRIPT_NAME'] == '/bitrix/admin/1c_exchange.php'){
      
        // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ts_exchange_update.txt', print_r($arFields, true), FILE_APPEND);

        if(!MSETools::testPriceTypeInException($arFields['CATALOG_GROUP_ID'])){
          return false;
        }
    }
    // $arFields['CATALOG_GROUP_ID']
  	// file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tststs.txt', print_r($arFields['CATALOG_GROUP_ID'], true), FILE_APPEND);
   //  file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tststs.txt', print_r($_SERVER['SCRIPT_NAME'], true), FILE_APPEND);

  }

  function controlPriceUpdateSynch($ID, &$arFields){
    if($_SERVER['SCRIPT_NAME'] == '/bitrix/admin/1c_exchange.php'){
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ts_exchange_update.txt', print_r($arFields, true), FILE_APPEND);
      if(!MSETools::testPriceTypeInException($arFields['CATALOG_GROUP_ID'])){
        return false;
      }
    }
      // $arFields['CATALOG_GROUP_ID']
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tststs.txt', print_r($arFields['CATALOG_GROUP_ID'], true), FILE_APPEND);
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tststs.txt', print_r($_SERVER['SCRIPT_NAME'], true), FILE_APPEND);
  }

  function controlPriceDeleteSynch($ID){
    if($_SERVER['SCRIPT_NAME'] == '/bitrix/admin/1c_exchange.php'){
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ts_exchange_update.txt', print_r($ID, true), FILE_APPEND);
        $arPriceInfo = CPrice::GetByID($ID);
        if(!MSETools::testPriceTypeInException($arPriceInfo["CATALOG_GROUP_ID"])){
          return false;
        }
    }
    
  }

  $eventManager->addEventHandler('aspro.optimus', "OnAsproGetTotalQuantity",'setActualTotalCount');
  function setActualTotalCount($arItem, &$currentAmount){
    global $USER;
    global $iCurCityID;
    //$arItem['STORES_COUNT'] // Общее количество на складах.
      $idCurrentItem = $arItem['ID'];
      $dbCityInfo = CIBlockElement::GetList(
        array(),
        array("IBLOCK_ID"=>18, "ID"=>$iCurCityID),
        false,false,
        array("ID","PROPERTY_STORES"))->Fetch();

      $idCurrentStore = $dbCityInfo['PROPERTY_STORES_VALUE'] ? explode("###", $dbCityInfo['PROPERTY_STORES_VALUE']) : 1; 

      $rsStore = CCatalogStoreProduct::GetList(
        array(), array('PRODUCT_ID' => $idCurrentItem, 'STORE_ID' => $idCurrentStore), 
        false, false, array('AMOUNT'))->Fetch(); 
      
      $currentAmount = $rsStore['AMOUNT'];

  }

  $eventManager->addEventHandler('main', "OnBeforeEventSend",'changeLinkInPaidSend');
  function changeLinkInPaidSend(&$arFields, &$arTemplate)
  {
    
    if($arTemplate['EVENT_NAME'] == 'SALE_ORDER_PAID'){

      $idOrder = $arFields['ORDER_ID'];
      $arOrder = CSaleOrder::GetByID($idOrder);
      $idUser = $arOrder['USER_ID']; 
      $arUserInfo = CUser::GetByID($idUser)->Fetch();
      $idCityUser = $arUserInfo['UF_CLOSE_CITY']; 
      $dbCityInfo = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>18, "ID"=>$idCityUser),false,false,array("CODE"))->Fetch(); 
      $userCodeCity = $dbCityInfo['CODE']; 
      $userLinkServer = "$userCodeCity.memstech.ru";
      $arFields['USER_SERVER_PATH'] = $userLinkServer; 
    }

  }

  // $eventManager->addEventHandler('catalog', "OnBeforePriceUpdate",'testUpdatePriceSynch');
  // CModule::IncludeModule("catalog"); 
  // $eventManager->addEventHandler('catalog', "OnBeforePriceAdd", 'testAddPriceSynch');
  // $eventManager->addEventHandler('catalog', "OnBeforePriceDelete",'testDeletePriceSynch');

  // function testDeletePriceSynch($ID){

  //     file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_delete_sy.txt', print_r($ID, true), FILE_APPEND);
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_delete_sy.txt', "\n", FILE_APPEND);
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_delete_sy.txt', print_r($_SERVER['SCRIPT_NAME'], true), FILE_APPEND);
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_delete_sy.txt', "\n", FILE_APPEND);
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_delete_sy.txt', "------------------------------------------\n", FILE_APPEND);

  // }

  // function testAddPriceSynch($arFields){
      // print_r('asdasdasd'); 
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_add_sy.txt', print_r($arFields, true), FILE_APPEND);
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_add_sy.txt', "\n", FILE_APPEND);
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_add_sy.txt', print_r($_SERVER['SCRIPT_NAME'], true), FILE_APPEND);
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_add_sy.txt', "\n", FILE_APPEND);      
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_add_sy.txt', "------------------------------------------\n", FILE_APPEND);

  // }

  // function testUpdatePriceSynch($ID, &$arFields){

  //     file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_update_sy.txt', print_r($arFields, true), FILE_APPEND);
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_update_sy.txt', "\n", FILE_APPEND);
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_update_sy.txt', print_r($_SERVER['SCRIPT_NAME'], true), FILE_APPEND);
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_update_sy.txt', "\n", FILE_APPEND);      
      // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/tprice_update_sy.txt', "------------------------------------------\n", FILE_APPEND);

  // }
  

  function debug6000($content){
    global $USER;
    if($USER->IsAdmin()){
      echo '<pre>';
        print_r($content);
      echo '</pre>';
    }
  }

  

?>
