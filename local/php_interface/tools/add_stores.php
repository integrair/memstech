<?

class CIBlockPropertyOutherProperty
{


	function GetUserTypeDescription()
	{

		return array(

	            'PROPERTY_TYPE' => 'S',
	            'USER_TYPE' => 'IB_CATALOG_PROP_STORE',
	            'DESCRIPTION' => 'Привязка к складу',
	            'GetPropertyFieldHtml' => array('CIBlockPropertyOutherProperty','GetPropertyFieldHtml'),
	            'ConvertToDB' => array('CIBlockPropertyOutherProperty','ConvertToDB'),
	            'ConvertFromDB' => array('CIBlockPropertyOutherProperty','ConvertFromDB')
	    );
	}



	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
    		CModule::IncludeModule('catalog');
    		$arStores = CCatalogStore::GetList(array(), array(), false, false, array('ID','TITLE'));
    		$arrayStore = array();
    		while($rstr = $arStores->Fetch()){
    			$arrayStore[] = $rstr;
    		}
    		$array_current_value = explode("###", $value['VALUE']);
			$result .= "<select multiple name='".$strHTMLControlName[ "VALUE" ]."[]' id='".$strHTMLControlName[ "VALUE" ]."'>";
			foreach ($arrayStore as $arSt) {
				$result .= '<option';
				if(in_array($arSt['ID'], $array_current_value)){
					$result .= ' selected';
				}
				$result .= ' value="'.$arSt['ID'].'">'.$arSt['TITLE'].'</option>';	
			}
			$result .= "</select>";
           
            return $result; 
    }
         
    function ConvertToDB($arProperty, $value)
    {
    		
    		$value = implode("###", $value['VALUE']);
            return $value;
    }
         
    function ConvertFromDB($arProperty, $value)
    {
            
            $return = array();
            $return["VALUE"] = $value["VALUE"];
            return $return;
    }


}

?>
