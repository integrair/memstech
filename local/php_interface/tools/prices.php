<?

class CIBlockPropertyTypePrices
{


	function GetUserTypeDescription()
	{


		return array(

	            'PROPERTY_TYPE' => 'S',
	            'USER_TYPE' => 'IB_CATALOG_TYPE_PRICE',
	            'DESCRIPTION' => 'Привязка к типу цены',
	            'GetPropertyFieldHtml' => array('CIBlockPropertyTypePrices','GetPropertyFieldHtml'),
	            'ConvertToDB' => array('CIBlockPropertyTypePrices','ConvertToDB'),
	            'ConvertFromDB' => array('CIBlockPropertyTypePrices','ConvertFromDB')
	    );
	}


	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {

    		CModule::IncludeModule('catalog');
    		$arPrices = CCatalogGroup::GetList();
    		$arrayPrices = array();
    		while($rp = $arPrices->Fetch()){
    			$arrayPrices[] = $rp;
    		}
    		
			$result .= "<select name='".$strHTMLControlName[ "VALUE" ]."' id='".$strHTMLControlName[ "VALUE" ]."'>";
			$result .= '<option value="">--пусто--</option>';
			foreach ($arrayPrices as $arP) {
				$result .= '<option';
				if($value['VALUE'] == $arP['ID']){
					$result .= ' selected';
				}
				$result .= ' value="'.$arP['ID'].'">';
				if($arP['NAME_LANG']){
					$result .= $arP['NAME_LANG'];	
				}else{
					$result .= $arP['NAME'];	
				}
				$result .= '</option>';	
			}
			$result .= "</select>";
           
            return $result; 
    }
         
    function ConvertToDB($arProperty, $value)
    {
    		
            return $value['VALUE'];
    }
         
    function ConvertFromDB($arProperty, $value)
    {
            
            $return = array();
            $return["VALUE"] = $value["VALUE"];
            return $return;
    }



}

?>
